---
layout: post
title: "Pushing a docker compose to Fargate"
permalink: /whatisir4/
date: 2022-03-04 
---
# Docker Compose away from how.

One of the problems with running Docker Compose on your local machine, is that you need extra steps to allow other people to access those containers, which itself introduces security questions.

Assuming you already have a AWS account (works with Azure ACI aswell), you can simply change the docker context to point to AWS Fargate and then just use 'docker compose up' and down.

For an example, and instructs to do this, head over to my github repo: https://github.com/eagle840/docker-compose-on-cloud  and review the read me, and an example docker compose file.


