---
layout: post
title: "My-First-Hackathon"
date: 2020-03-09
---

# My First Hackathon.

This weekend was spent at the Microsft Reactor site in London on my first hackathon. the first day was a little intimadating, but Sunday we powered through the project our team selected: designing, creating  and deploying a Microsoft PowerApp that scans a photo from your phone/tablet and determines if the construction workers in the pic are all wearing their 5 point PPE - using AI. In last the part of the day the teams spent describing and demo'ing their projects to the judges - and we came out in 3rd Place! we got got to select a prize from what was left in the prize table and I came home with a Google Home Mini".

Will  certainly be attending more hackathons in the future.

![Hack5 3rd place](https://storage.cloud.google.com/nicholasbusby.vauxbuz.net/Hack5.jpg)

And here's the presention that won us 3rd place. https://www.youtube.com/watch?v=W5bhq0tLFeQ&list=PLM0EU9nRaeVBy9ByZ05yRi5Jz-xRB3IEv&index=4
