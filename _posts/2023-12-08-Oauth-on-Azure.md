---
layout: post
title: "Oauth on Azure"
permalink: /oauth/
date: 2023-12-08
---
# Using Oauth on auth

Oauth2 [rfc6749](https://datatracker.ietf.org/doc/html/rfc6749)

Oauth2 in gifs [site](https://dev.to/hem/oauth-2-0-flows-explained-in-gifs-2o7a)

Microsoft Identity Platform is OpenIC connect compliant [docs](https://learn.microsoft.com/en-us/entra/identity-platform/)

Getting started [MS Learn](https://learn.microsoft.com/en-us/training/modules/getting-started-identity/)

To get started, create App Reg, and then goto the Quickstart blade, it will provide many programming examples.

## Libraries

- MSAL for many languages [docs](https://learn.microsoft.com/en-us/entra/identity-platform/msal-overview), [link](https://learn.microsoft.com/en-us/azure/active-directory/develop/msal-client-applications)
- Microsoft.Identity.Web for .Net [docs](https://learn.microsoft.com/en-us/training/modules/getting-started-identity/) [example](https://github.com/Azure-Samples/active-directory-aspnetcore-webapp-openidconnect-v2/tree/master/1-WebApp-OIDC)
- ADAL - **deprecated** 
- 3rd party OpenID connect packages

## Clients Types

- Confidential Client; Clients capable of maintaining the confidentiality of their credentials.
- Public Client; Clients incapable of maintaining the confidentiality of their credentials.


## Tokens (implicit grant flow)

- ID Tokens: includes claims about the user, used by OIDC. A web app will use this to tell who you are
- Access Tokens: used for AuthZ for use with resources, you should never touch the token, just pass it along. Used with API's
- Refresh Tokens: to get new tokens when old ones expire.
- Additional custom tokens can be added in the App Registration, in the tokens blade.


|                     | ID Token                                      | Access Token                                 |
|---------------------|-----------------------------------------------|----------------------------------------------|
| Purpose             | Used for authentication and user identification | Used for authorization and accessing resources |
| Content             | Contains user information and claims           | Contains authorization details and scopes     |
| Audience            | Intended for the client application            | Intended for the resource server              |
| Expiration          | Typically short-lived (e.g., few minutes)      | Can be long-lived (e.g., hours, days)         |
| Usage               | Used by client application to verify identity  | Used by client application to access resources|
| Claims              | Contains user-specific claims (e.g., sub, iss) | Contains authorization-specific claims        |
| Authentication Level| May include authentication context information| Does not include authentication context       |
| Scope               | Does not have a concept of scope               | Contains scope(s) for resource access         |
| Token Type          | JWT (JSON Web Token)                           | Can be JWT, OAuth token, or other formats     |

## Token Grant Flows

[MD docs](https://learn.microsoft.com/en-us/entra/identity-platform/v2-oauth2-auth-code-flow)

| App Type       | OAuth Flow     | ID Token | Access Token |  links |
| -------------- | -------------- | -------- | ------------ |--------|
| Web App        | Authorization Code Flow | Yes | Yes |  [rfc](https://datatracker.ietf.org/doc/html/rfc6749#section-4.1)
| Mobile App     | Authorization Code Flow | Yes | Yes |
| Single Page App| Implicit Flow | Yes | Yes | [rfc](https://datatracker.ietf.org/doc/html/rfc6749#section-4.2)
| Desktop App    | Authorization Code Flow | Yes | Yes |
| Server-to-Server App | Client Credentials Flow | No | Yes |
| IoT App        | Device Flow | No | Yes |
| Command Line App | Resource Owner Password Credentials Flow | No | Yes |


### Learn

- Microsft https://learn.microsoft.com/en-us/training/paths/m365-identity-associate/
    This includes links to screen casks and github repos. You'll see them in the intro section of each module

### Playgrounds


- Oauth Flows https://www.oauth.com/playground/
- Curity Playground https://oauth.tools/
- Google specific https://developers.google.com/oauthplayground/
- MS specific https://developer.microsoft.com/en-us/graph/graph-explorer

### Learn and program

- Microsoft ASP.net repo that has examples of using OAuth flow with azure. [github](https://github.com/Azure-Samples/active-directory-aspnetcore-webapp-openidconnect-v2/blob/master/README.md)
- MS Docs authorization code flow  https://learn.microsoft.com/en-us/entra/identity-platform/v2-oauth2-auth-code-flow
- MS Dcos client credentials flow  https://learn.microsoft.com/en-us/entra/identity-platform/v2-oauth2-client-creds-grant-flow
- MS Docs device authorization grant flow https://learn.microsoft.com/en-us/entra/identity-platform/v2-oauth2-device-code
- On-Behalf-Of flow
- https://learn.microsoft.com/en-us/entra/identity-platform/v2-oauth2-implicit-grant-flow

#### Videos
- Detailed AuthZ code flow on Azure [concept works](https://www.youtube.com/watch?v=pfBc2EIbgQw)

## Authorization

**!Do not overengineer authorization!**

Two ways
- API; no ID token
- User

In azure there are 3 ways to AuthZ
- Groups; std AAD groups
- Custom Claims; ie custom tokens
- App Roles; defines at app level

That gives you  6 ways to AuthZ in Azure