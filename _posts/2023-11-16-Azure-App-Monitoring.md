---
layout: post
title: "Monitoring Azure Apps "
date: 2023-11-16
author: Nick Busby
permalink: /azureapp/
published: true
---

# Troubleshooting Azure App Services


* main page for App-Services <https://learn.microsoft.com/en-us/azure/app-service/>
* Logging page for App-service <https://learn.microsoft.com/en-us/azure/app-service/overview-monitoring>

There are many logs in Web Apps

See
 <https://learn.microsoft.com/en-us/training/modules/configure-web-app-settings/5-enable-diagnostic-logging>


__Enable application logging (Linux/Container)__
	
1. In App Service logs set the Application logging option to File System.
2. In Quota (MB), specify the disk quota for the application logs. In Retention Period (Days), set the number of days the logs should be retained.
3. When finished, select Save.

From <https://learn.microsoft.com/en-us/training/modules/configure-web-app-settings/5-enable-diagnostic-logging> 





__Stream logs__
Before you stream logs in real time, enable the log type that you want. Any information written to files ending in .txt, .log, or .htm that are stored in the /LogFiles directory (d:/home/logfiles) is streamed by App Service.

From <https://learn.microsoft.com/en-us/training/modules/configure-web-app-settings/5-enable-diagnostic-logging> 



__Access log files__

https://learn.microsoft.com/en-us/azure/app-service/troubleshoot-diagnostic-logs

If you configure the Azure Storage blobs option for a log type, you need a client tool that works with Azure Storage.
For logs stored in the App Service file system, the easiest way is to download the ZIP file in the browser at:
	• Linux/container apps: https://<app-name>.scm.azurewebsites.net/api/logs/docker/zip
	• Windows apps: https://<app-name>.scm.azurewebsites.net/api/dump

From <https://learn.microsoft.com/en-us/training/modules/configure-web-app-settings/5-enable-diagnostic-logging> 





