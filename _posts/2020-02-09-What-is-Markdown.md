---
layout: post
title: "IT in 2 minutes: what is markdown"
---

# What is MARKDOWN

In this 2 minutes review, i'll cover markdown, a simple language to produce simple but great looking pages, like this one. 

**John Gruber (creator):**

> "The overriding design goal for Markdown’s formatting syntax is to make it as readable as possible. The idea is that a Markdown-formatted document should be publishable as-is, as plain text, without looking like it’s been marked up with tags or formatting instructions."

- Checkout the original project site  <https://daringfireball.net/projects/markdown/>

- Arron has written a  little history on it and has a simple live markdown editor at:  
<http://aaronbeveridge.com/markdown/index.html>

- Or go for a fully feature live editor at:  <https://stackedit.io/>
- or <https://dillinger.io/>

More Resources:

> - [markdown rfc 7763](https://tools.ietf.org/html/rfc7763)
> - [markdown variations rfc 7764](https://tools.ietf.org/html/rfc776)
> - [wikipedia](https://en.wikipedia.org/wiki/Markdown)