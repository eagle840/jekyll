---
layout: page
title: "Function Frenzy: Unlocking the Possibilities with Azure Functions"
permalink: /AzureFunctions/
date: 2024-01-03
---

# Function Frenzy: Unlocking the Possibilities with Azure Functions

## Main Docs

- https://learn.microsoft.com/en-us/azure/azure-functions/


## Main Types

Run multiple different types of run time stacks on Windows, Linux or Docker

1 Million executions/month free

### Regular

Plans:
- Consumption (Serverless) - no network
- Functions Premium ($vCPu & Gb/HR) - Event based scaling and network isolation, ideal for workloads running continuously.
- App Service Plan

## Using Managed Identities


- turn on
- Subscription > IAM > Add Role assignment for the items you want to control.

### Durable Functions (stateful)


## Development

#### Options
- VsCode
- Any editor + Core tools
- Develop in portal

Only certain languages are supported in the portal, the others will need to be developed locally and published to the function App. [docs](https://learn.microsoft.com/en-us/azure/azure-functions/functions-create-function-app-portal)

- VSC Azure Functions Extension [Docs & Install](https://learn.microsoft.com/en-us/azure/azure-functions/functions-develop-vs-code?tabs=node-v3%2Cpython-v2%2Cisolated-process&pivots=programming-language-csharp)
- Core Tools  $ func  [Docs & Install](https://learn.microsoft.com/en-us/azure/azure-functions/functions-run-local?tabs=windows%2Cisolated-process%2Cnode-v4%2Cpython-v2%2Chttp-trigger%2Ccontainer-apps&pivots=programming-language-csharp)