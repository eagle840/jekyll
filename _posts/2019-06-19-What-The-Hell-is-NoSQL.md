---
layout: post
title: "IT in 2 minutes: What  the hell is NoSQL"
---

# What the hell is NoSQL

### NoSQL in 2 minutes for IT pros

I'm sure you have heard and possibly used SQL, or it's real name RDBMS - Relational Database Mamagemet System.

SQL is the language you use to query that database, but most producers of RDBMS love to include SQL in their product name. For example Microsoft SQL, MySQL. But that query language is used in many other database types - but not all of them.

And how about NoSQL? Someplaces you'll hear 'non SQL' or 'non Relational' But I perfer 'Not Only' - because you can use SQL on some of the other non-'sql' DB's.

I'm going to split into 4 major database class':

KEY VALUE: The simplest of the 4, and the fastest. Just give it a key and a value (EG: Name & Nicholas) and then ask for the value when needed (eg hey DB, whats the value of Name? Oh thats easy - it's Nicholas). It's great for caching results in memory. See the link at the bottom of the page for a rank of the best KV db's - and others.

DOCUMENT DB's: A little bit more completed, It stores JSON (and related) documents, basicly a snippet of data, one after the other, but it doesn't care much about the format of data (unlike RDBMS) but unlike a K:V db, document DBs can analysis the data in the JSON documents.

GRAPH DB's: This is the cool one of the bunch, data is in the form of nodes and relations, betweeen those nodes you can find some pretty complicated relations between different nodes to help your business grow ensites.

TABULAR/WIDE/COLUMNAR Store DB: This one is just like a SQL table or spread sheet table, but (usually) a really long one and its usually not tied to any others. It's great for holding things like logs.

And for a bonus, Many on the cloud providers (EG Aws, Azure) provide these types of DB's already setup for you to jump in, a few even mix some of the databases into one product.

What to find out the lastest databases and info about them - head over to db-engines: [db-engines.com](https://db-engines.com/en/ranking)