---
layout: post
title: "new katacoda lab: Running statefulsets on K8S"
---

# Statefulsets on Kubernetes

Just posted my new lab on Katacoda.  

In this lab we'll setup a NFS server to work as a K8S storage class, and then setup a nginx statement.

[Statefulsets on Kubernetes](https://www.katacoda.com/ir4engineer/scenarios/statefulsets)