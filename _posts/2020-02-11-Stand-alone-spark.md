---
layout: post
title: "Katacoda-Lab-Running-Spark-On-A-Single-Server"
---

# Do you have the Spark for Data Engineering?

Spark is one of the premier opensource data processing tools and is used extensively in data engineering and data science. Learn how to install it on a Linux Server and test out some Scala and Python programming on it at new Spark Lab. #spark #dataengineering #datascience #python

<https://www.katacoda.com/ir4engineer/scenarios/singlespark>
