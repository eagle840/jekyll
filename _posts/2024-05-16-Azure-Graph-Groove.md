---
layout: page
title: "Azure Graph Groove: Dancing Through Azure Graph"
permalink: /AzureGraph/
date: 2024-05-16
---


# Azure/Microsoft Graph

Head over to the Azure Resource Graph Explorer in the portal



## KQL

See the kql page, it is a big part of Graph

**KQL in Graph**

see the following page to jump quickly into understanding https://learn.microsoft.com/en-us/azure/governance/resource-graph/concepts/query-language
 - also includes a list of tables the graph can access
 - do a '| distinct  type' on a table

## Format

```
<tableName>
// this is a comment
| where 
| <operatoers>
| project name, value
```

## Example KQL on Azure tables

- Example queries: https://learn.microsoft.com/en-us/azure/governance/resource-graph/samples/samples-by-table?tabs=azure-cli
- List of tables: https://learn.microsoft.com/en-us/azure/governance/resource-graph/reference/supported-tables-resources

## KQL Operators

- mv-expand (mvexpand)  [link](https://learn.microsoft.com/en-us/azure/data-explorer/kusto/query/mv-expand-operator)

## Azure Graph (Azure Resource Graph Explorer)

Main Page(In Azure) [link](https://azure.microsoft.com/en-us/get-started/azure-portal/resource-graph/)
Main Page(in governance?) [link](https://learn.microsoft.com/en-us/azure/governance/resource-graph/)

### Scope

- usually the directory you're in
- Options: Directory,   Mgmnt Grp,   Subscription

### Categories Tab

- lists available tables
- lists **types** in each table

### Tables Tab

- lists available tables
- lists **types** in each table
- common tables
  - **resources**: a comprehensive collection of all your Azure resources
  - **resourcecontainers** information about resource containers, such as subscriptions, resource groups, and management groups. This table, when used in conjunction with the resources table, allows you to manage and organize your Azure resources more effectively by offering comprehensive details like container IDs, names, types, and locations alongside the specific properties of each resource.

### Query tab


- [Web tool](https://developer.microsoft.com/en-us/graph/graph-explorer)
- [documentation](https://learn.microsoft.com/en-us/graph/)
- [Intro Page](https://developer.microsoft.com/en-us/graph/)
- [MS Learn Fundamentals Path](https://learn.microsoft.com/en-gb/training/paths/m365-msgraph-fundamentals/)
- [MS Learn Graph related modules](https://learn.microsoft.com/en-gb/training/browse/?products=ms-graph)
- [Quick Start Apps](https://developer.microsoft.com/en-us/graph/quick-start)
- In Azure, look for **Azure Resource Graph Explorer** - but this is a read-only query on Azure resources


## Flow of data in MS Graph

1) MS Graph API ; do REST calls to get data in and out   
2) MS Graph Connectors ; incoming data to store in MS Graph   
3) MS Graph Data Connect ; Send data to Azure data Services   

## Data Stored in Graph

- M365 Core
- EMS
- Windows
- D365
- Governance [docs](https://learn.microsoft.com/en-us/azure/governance/resource-graph/samples/advanced?tabs=azure-cli#show-resource-types-and-api-versions)

## Resources

- John Savill on Graph [youtube](https://www.youtube.com/watch?v=gkOh4MjhxIs)
- Azure API endpoint with [Azure Resource ExplorerPermalink](https://resources.azure.com/)
- Best KQL practice [Best practices for Kusto Query Language queries](https://learn.microsoft.com/en-us/azure/data-explorer/kusto/query/best-practices)

## notes

### pulling tags

```
ResourceContainers
| where type =~ 'microsoft.resources/subscriptions/resourcegroups'
| mvexpand tags
| project name, tags
```

returns a table with name and separate k:V on eatch line, while repeating names

Now turn that json k:v into strings in their own column with 'extend' cmd

Now that it is in plain text, you can search

```
ResourceContainers
| where type =~ 'microsoft.resources/subscriptions/resourcegroups'
| mvexpand tags
| extend tagKey = tostring(bag_keys(tags)[0])
| extend tagValue = tostring(tags[tagKey])
| where tagKey =~ "Team"
| summarize count() by tagValue
```
---

### query tls version
```
// https://learn.microsoft.com/en-us/azure/governance/resource-graph/samples/samples-by-category?tabs=azure-cli#list-azure-app-service-tls-version
AppServiceResources
| where type =~ 'microsoft.web/sites/config'
| project id, name, properties.MinTlsVersion
```