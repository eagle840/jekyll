---
layout: post
title: "Architecture with UK Gov Standards"
date: 2023-10-20
---


# Architecture with UK Gov Standards


A list of references for getting cloud architecture to UK Gov standards


- **GDS Service Manual** - create and run great public services that meet the Gov  Service Standard. [Web page](https://www.gov.uk/service-manual)
  - The Central Digital and Data Office (CDDO), the Geospatial Commission, the Government Digital Service (GDS) and the Incubator for Artificial Intelligence (i.AI) and have merged to create the new Government Digital Service

- **GDS API design guidance** - designing, building and running  [web site](https://www.gov.uk/government/collections/api-design-guidance)

- **The GDS Way** - guides teams to build and operate brilliant, cost-effective digital services. [website](https://gds-way.digital.cabinet-office.gov.uk/#the-gds-way)

- **Data Standards Authority**- improve how the public sector manages data [web page](https://www.gov.uk/government/groups/data-standards-authority)

- **Service Standard** - create and run great public services. [web Page](https://www.gov.uk/service-manual/service-standard)

- **Data Standards** -  establishing standards to make it easier and more effective to share and use data across government. [website](https://www.gov.uk/government/groups/data-standards-authority) [github](https://github.com/alphagov/data-standards-authority)

- **Code of Practice** - Set of criteria to help government design, build and buy technology [webpage](https://www.gov.uk/guidance/the-technology-code-of-practice)

- **GDS Developer Docs** - This is the technical documentation for the GOV.UK team in the Government Digital Service [webpage](https://docs.publishing.service.gov.uk/)

- **UK Gov style** - UK Giv styles, components and patterns [webpage](https://design-system.service.gov.uk/)

## Metrics

SRE vs Uk Gov KPI's

- SRE Four Golden metrics [Google SRE link](https://sre.google/sre-book/monitoring-distributed-systems/)
- UK Gov KIP's [UK Gov Link](https://www.gov.uk/service-manual/measuring-success/using-data-to-improve-your-service-an-introduction)

## Software Documentation

The UK government has a standard for software documentation. The Government Technology Standards and Guidance provides a set of criteria to help government design, build, and buy technology [url](https://www.gov.uk/guidance/government-technology-standards-and-guidance). 

The Technology Code of Practice is a part of this guidance and provides a set of standards for technology development and procurement 1.
The code of practice includes guidelines for 
 - accessibility, APIs, application development, buying technology, cloud strategy, data protection, data provisioning and usage, design and build government services, design principles, digital inclusion, digital marketplace, digital outcomes and specialists framework, digital service standard, digital transformation, emerging technologies, end-user devices, GOV.UK Notify, GOV.UK Pay, GOV.UK Proposition, GOV.UK Platform as a Service (PaaS), GovWifi, green technology, hosting your service, identity assurance, job roles, legacy, managing government websites, Microsoft 365 guidance, networking, open source, open standards, performance platform, public service network (PSN), registers, risk management, smarter working, security, talent acquisition, telecommunications, and user research 

The UK government also has a policy for selecting open standards for software interoperability, data, and document formats in government IT [link](https://www.gov.uk/government/publications/open-standards-principles/open-standards-principles). The policy guides departments on how to implement open standards and provides a list of open standards that are recommended for use in government IT [link](https://www.gov.uk/government/publications/open-standards-principles).


## Other

- **Blog on 'docs as code'** - from the Gov technology blog [webpage](https://technology.blog.gov.uk/2017/08/25/why-we-use-a-docs-as-code-approach-for-technical-documentation/)

- **Tech Docs Template** - build technical documentation using a GOV.UK style. [github](https://github.com/alphagov/tech-docs-template#tech-docs-template)

- **X-Gov** - unofficial GOV.UK tool [github](https://github.com/x-govuk)

- **Government Digital Service** -   [webpage](https://gds.blog.gov.uk/)  [github](https://github.com/alphagov)

- **Government Central Digital and Data Office** - [web site](https://www.gov.uk/government/organisations/central-digital-and-data-office)

- **Government Secure by Design** - [webpage](https://www.security.gov.uk/guidance/secure-by-design/principles/)
  - blog on SbyD [bliog](https://cddo.blog.gov.uk/2023/05/05/welcome-to-the-cross-government-secure-by-design-approach/)

- **UK Gov Json Schemas** - [webpage](https://docs.publishing.service.gov.uk/content-schemas.html)

- **UK Gov Security** - [website](https://www.security.gov.uk/about-us/)

- **OneLogin** - [docs](https://www.sign-in.service.gov.uk/documentation) [UK GDS Blog](https://gds.blog.gov.uk/category/govuk-onelogin/)

- **Forms Service** - https://www.forms.service.gov.uk/

- **UK Incubator for Artificial Intelligence** - [home page](https://ai.gov.uk/) and [project page](https://ai.gov.uk/projects/)

- **Prototyping Kit** Look and feel like GOV.UK pages. [Getting Started](https://prototype-kit.service.gov.uk/docs/install/getting-started)


# Department Specific

## Dfe

- [DfE Technical Guidance](https://technical-guidance.education.gov.uk/)