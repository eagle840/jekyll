---
layout: post
title: "(updated) Installing Linux on Windows 10 using WSL2"
---

# Installing WSL 2 on Windows (1903 & 1904)

Since my last blog on installing WSL on Windows 10 (2004), Microsoft have released a KB to allow it run on 1903 and 1904.  

To find the version your running, run the cmd: winver

Just install the KB at https://support.microsoft.com/en-us/help/4566116/windows-10-update-kb4566116, be sure to install the correct one for your windows verion and the correct architecture.   

This time I've included the  dism commands to save you running through the settings menu.  

1. Install WSL:  dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart   
2. for 1903/9 dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart   
OR for 2004   dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart 
3. Download/install the Kernel Updrade at https://aka.ms/wsl2kernel  
4. wsl --set-default-version 2   
5. Then install your favorite Linux version through the Windows Store.   

Ref: https://devblogs.microsoft.com/commandline/wsl-2-support-is-coming-to-windows-10-versions-1903-and-1909/   


#  An intergrated Linux solution on Windows. (from 2020-06-23)

For a long time now users have been able to run Linux through a VM on Windows, and then with the intergration of WSL it provided to be a smoother experience, however Mircosoft has made a few changes so that now runs virtualizes - WSL2, signifcately speeding up it execution. 

In this blog I'll be covering the installation to get it up and running.

1. confirm and turn on Virualization in the BIOS
2. update your version on Windows 10 (it can be home edition) to 2004 (run winver)
3. Run 'Turn Windows features on or off' and turn on: Virtial Machine Platform  and Windows Subsystem for Linux
4. Goto the Windows Store and install/launch Ubuntu 20.04 (or your perferred version)
5. Download/install the Kernel Updrade at https://aka.ms/wsl2kernel
6. Open a powershell prompt and run: 'wsl -l -v'  
7. run wsl --set-version Ubuntu-20.04 2  # to set the Kernel to verion 2
8. run wsl --set-default-version 2  # to make version 2 every time
(on my system I had to remove/restart/install 'Virtial Machine Platform' again to set the kernel version to 2)
9. Type ubuntu in the search bar and it should run.