---
layout: post
title: "Setting up Azure AAD and Web apps"
date: 2023-10-20
---

# Azure App Registrations and Web Apps


An App registeration tied together the trust between an App and AAD

The Redirect URL is where the AuthN returns to after the user signs in
- eg localhost, you're running an app on your local machine
- eg www.myapp.com, running a service on the interwebs
- can be multiple
- the AuthN will not return you to an unlisted RedirectURL


The 'App Reg' is the Global definiation, the ObjectID  
- The link to Ent App is: Overview (Managed Application in local Directory)  
 
The 'Ent App' has/is the service princable and it's settings? It also has a different ObjectID, but the AppID is the same
- The link to the App Reg is the the Owners Blade



# Setting a SP with scope

Work with service principals [ms docs](https://learn.microsoft.com/en-us/cli/azure/azure-cli-sp-tutorial-1?tabs=bash)

az ad sp create-for-rbac --name "{sp-name}" --sdk-auth --role contributor --scopes /subscriptions/{subscription-id}

- you can set scopes at different levels, default is no role/scope
- MS docs [AZ AD SP](https://learn.microsoft.com/en-us/cli/azure/ad/sp?view=azure-cli-latest)

example output for

'az ad sp create-for-rbac --name "myApp1210" --role contributor --scopes /subscriptions/xxssxxss/resourceGroups/njb1210 --sdk-auth``

note that the object ID is not in the output.
```
{
  "clientId": "xxclientidxx",   # App Reg
  "clientSecret": "xxsecretvaluexx",
  "subscriptionId": "xxssxxss",
  "tenantId": "xxtenantidxx",
  "activeDirectoryEndpointUrl": "https://login.microsoftonline.com",
  "resourceManagerEndpointUrl": "https://management.azure.com/",
  "activeDirectoryGraphResourceId": "https://graph.windows.net/",
  "sqlManagementEndpointUrl": "https://management.core.windows.net:8443/",
  "galleryEndpointUrl": "https://gallery.azure.com/",
  "managementEndpointUrl": "https://management.core.windows.net/"
}
```

# Enterprise Apps Blade

Note the apps are filtered to 
- 'Enterprise Applications', (default)   ?Web Apps?
- 'MS applications'   commerical apps
- 'Managed Identities'  created my azure resources?
- 'All Applications' lists all, including SPs not in the above ince 'az ad sp create'

Every one has a 'ObjectID' and a 'ApplicationID'

# Identity providers

| Provider                        | Sign-in endpoint          | How-To guidance          |
|---------------------------------|---------------------------|--------------------------|
| Microsoft Identity Platform     | /.auth/login/aad          | [Microsoft Identity Platform](https://learn.microsoft.com/en-us/azure/container-apps/authentication-azure-active-directory) |
| Facebook                        | /.auth/login/facebook     | [Facebook](https://learn.microsoft.com/en-us/azure/container-apps/authentication-facebook)                 |
| GitHub                          | /.auth/login/github       | [GitHub](https://learn.microsoft.com/en-us/azure/container-apps/authentication-github)                   |
| Google                          | /.auth/login/google       | [Google](https://learn.microsoft.com/en-us/azure/container-apps/authentication-google)                   |
| Twitter                         | /.auth/login/twitter      | [Twitter](https://learn.microsoft.com/en-us/azure/container-apps/authentication-twitter)                |
| Any OpenID Connect provider     | /.auth/login/<providerName> | [OpenID Connect](https://learn.microsoft.com/en-us/azure/container-apps/authentication-openid)           |

# App blade vs Ent balde

| App Reg Blade            |  Ent App Blade         |
|--------------------------|------------------------|
|  App Id                  | App ID |
|  Object Id               | Object Id |
|  Tenant Id               | |
|  Supported acc types     | homepage url |
|  Client credientials     | |
|  Redirect URLs(spa,client, web)   | Owners |
|  App Id URL              | roles and admins |
|  Managed App Id  -----> | |
| | users and groups |
| | SSO |
| | provisioning |
| | <---- Properties blade, link to 'your tenant' app reg |