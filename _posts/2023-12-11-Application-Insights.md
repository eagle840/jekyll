---
layout: post
title: "Azure Application Insights"
permalink: /appsights/
date: 2023-12-11
---

# Azure Application Insights

Azure Application Insights is a powerful monitoring and analytics service provided by Microsoft Azure. It helps you monitor the performance and availability of your applications, gain insights into user behavior, and diagnose issues quickly.

## Monitor

Azure Monitor is a comprehensive monitoring solution that provides a unified view of your applications, infrastructure, and network. It allows you to collect and analyze telemetry data from various sources, including Application Insights.

Learn more about Azure Monitor [here](https://learn.microsoft.com/en-us/azure/azure-monitor/).

## Consumption resources

Azure Application Insights offers a wide range of features and resources to help you monitor and analyze your applications effectively. Some of the key resources include:

- **InSights**: Monitor your app, container, VM, VNet, and more. Check out the [list of resources](https://learn.microsoft.com/en-us/azure/azure-monitor/insights/insights-overview) with documentation.
- **Visualize**: Create workbooks, dashboards, and integrate with PowerBI and Grafana.
- **Analyze**: Use Metric Explorer, Log Analytics, and Change Analyze to gain insights from your telemetry data.
- **Respond**: Leverage AIOps, set up alerts and actions, and automate scaling.

You can find more information about these resources in the [az-204 module](https://learn.microsoft.com/en-us/training/modules/monitor-app-performance/).

# App Application Insights

Azure Application Insights offers a range of features to help you monitor and analyze your applications effectively. Some of the key features include:

- **Live Metrics**: Get real-time insights into the performance and availability of your applications.
- **Availability**: Monitor the uptime and responsiveness of your applications.
- **Github/Devops Integration**: Seamlessly integrate with your development and deployment workflows.
- **Usage**: Understand how your applications are being used by your users.
- **Smart Detectors**: Automatically detect and diagnose issues in your applications.
- **Application Map**: Visualize the dependencies and relationships between different components of your applications.
- **Distributed Tracing**: Trace requests across different services and components of your applications.

# AutoInstrumentation

Azure Application Insights supports auto-instrumentation, which means you don't need to make any code changes, access source code, or make configuration changes to start monitoring your applications. It eliminates the need for ongoing SDK update maintenance.

Learn more about the supported environments, languages, and resource providers for auto-instrumentation [here](https://learn.microsoft.com/en-us/azure/azure-monitor/app/codeless-overview#supported-environments-languages-and-resource-providers). You can also find information about tracking custom events and metrics using the Application Insights API [here](https://learn.microsoft.com/en-gb/azure/azure-monitor/app/api-custom-events-metrics).

# Insights SDK

If you are looking for language-specific SDKs for Azure Application Insights, you can find them in the [Microsoft GitHub repositories](https://github.com/orgs/microsoft/repositories?q=ApplicationInsights-&type=all&language=&sort=). These SDKs provide easy integration with your applications and enable you to collect and analyze telemetry data.

To enable correlation in your applications, you can follow the instructions [here](https://learn.microsoft.com/en-us/azure/azure-monitor/app/distributed-trace-data#enable-w3c-distributed-tracing-support-for-web-apps). Setting 'enableCorsCorrelation' to true enables correlation between different services and components.

# Application Maps

Application Maps in Azure Application Insights allow you to visualize the dependencies and relationships between different components of your applications. It uses the 'Cloud Role Name', 'roleName', and name to create the map. [link](https://learn.microsoft.com/en-us/azure/azure-monitor/app/app-map?tabs=net)

# Profiler

Capture, identify, and view performance traces for your application running in Azure, regardless of the scenario. [docs](https://learn.microsoft.com/en-us/azure/azure-monitor/profiler/profiler-overview) [video](https://www.youtube.com/watch?v=cghbGy6GPTI)

# OpenCensus (Opentelemetry)

If you want to use OpenCensus or Opentelemetry for monitoring your applications with Azure Application Insights, you can get started [here](https://learn.microsoft.com/en-us/azure/azure-monitor/app/opentelemetry-enable?tabs=aspnetcore).

# Logs (monitor)

Azure Application Insights allows you to monitor logs from your applications. Log-based metrics are translated into Kusto queries from stored events, while standard metrics are stored as pre-aggregated time series.

# Other

For more information on tracking exceptions and custom events using the Application Insights API, you can refer to the documentation [here](https://learn.microsoft.com/en-gb/azure/azure-monitor/app/api-custom-events-metrics#trackexception).