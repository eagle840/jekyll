---
layout: post
title: "new katacoda lab: Intro to SQL on Docker"
---

# Katacoda Lab: Intro to SQL on Docker

I have a new lab out on my Katacoda page: 'Intro to SQL on docker.' Quickly setup multiple SQL servers on a single machine and an administration container running Adminer all with the power of docker and docker-compose.

[Intro to SQL on Docker](https://www.katacoda.com/ir4engineer/scenarios/sql)