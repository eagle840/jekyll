---
layout: post
title: "Katacoda Lab: Network troubleshooting with TCPdump, Netcat and Wireshark"
---

# Katacoda Lab: Network troubleshooting with TCPdump, Netcat and Wireshark

Interested in finding out how to use some basic linux networking tools, tcpdump, netcat and wireshark?  
Head over to my newly designed lab at

https://katacoda.com/ir4engineer/scenarios/tcpdumpnc