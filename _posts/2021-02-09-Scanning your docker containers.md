---
layout: post
title: "Scanning your Docker containers for security issues"
---



I've just added a new Katacoda lab on  how to scan your docker containers for security issues with Trivy.

## What is Trivy

Trivy is a vulnerability and misconfiguration scanner that is 'looked after' my Aqua, it's designed to help speed checking container/images before pushing them out to production.  Check out the trivy github page below for the readme page for more detail on how it works.

## Katacoda like:  https://www.katacoda.com/ir4engineer/scenarios/container_cve



- Aqua's site:   https://www.aquasec.com/products/trivy/
- Opensource code at:  https://github.com/aquasecurity/trivy

