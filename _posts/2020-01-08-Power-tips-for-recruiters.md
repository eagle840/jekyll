---
layout: post
---
# Power Tip for recruiters/companies.

"Devops" is a undefined set of practices and procedures,  and doesn't apply to any specific technology stack. 

Be sure to include the technology stack in the heading when posting your Job Description.
eg.  Devops enginer in <X>,  X: [Java, Azure, etc.]