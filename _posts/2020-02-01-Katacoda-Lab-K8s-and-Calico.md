---
layout: post
title: "new katacoda lab: Running K8S with Calico"
---

# Katacoda Lab: Using Calico on kubenetes

For those of you working on your Kubernetes CKA, or looking to understand K8S networking better, I have a new lab up, 'Using Calico on Kubernetes'. I'm finishing up the last parts/bugs in the lab so any suggestions please let me know.The lab covers K8S, docker, CNI settings, Linux network switches, and troubleshooting images. Have fun, and remember if you break it, just restart the lab.

[Using Calico on kubenetes](https://www.katacoda.com/ir4engineer/scenarios/calico)