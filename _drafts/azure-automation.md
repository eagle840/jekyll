# Azure Automation

- https://learn.microsoft.com/en-us/azure/automation/


## Azure Automation Account

- sets in a RG
- !can have same name in differen resource groups
- Usually create a 'Run As' service principle
  - default is contributor at subscription level
  - can be adjusted

## Runbook

- webhook address's are only shown once - SAVE THEM

## Gallery

- https://github.com/azureautomation/runbooks

### Portal editor

- CMDLETS
- RUNBOOKS
- ASSETS

- NOTE the **Test pane** to test your script
- Not forget to publish

## Starting

### Webhook

- https://learn.microsoft.com/en-us/training/modules/explore-azure-automation-devops/6-examine-webhooks
- https://learn.microsoft.com/en-us/azure/automation/automation-webhooks?tabs=portal

### Schedule
  

