# Azure Policy

A policy definition expresses a condition to evaluate and the actions to perform when the condition is met.

policy/initive  --- assignment --->  Scope

[docs](https://learn.microsoft.com/en-us/azure/governance/policy/)

[samples on github](https://github.com/Azure/azure-policy/tree/master/samples)
[builtin](https://learn.microsoft.com/en-us/azure/governance/policy/samples/built-in-initiatives)
[quick start](https://learn.microsoft.com/en-us/azure/governance/policy/assign-policy-portal)