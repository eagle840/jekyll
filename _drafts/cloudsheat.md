cloud IaC cheat sheet


cli install and login
- aws:
    1. download, extract, 'sudo ./aws/install', test 'aws --version', get an access and a secret key from your account from the web portal, 
        run 'aws configure', enter keys, test 'aws s3 ls'
- Azure


Docker cloud providers CLI images
- AWS
    1. docker run --rm -it amazon/aws-cli command
    2. docs: https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-docker.html
- Azure 
    1. docker run -it mcr.microsoft.com/azure-cli
    2. docs: https://docs.microsoft.com/en-us/cli/azure/run-azure-cli-docker
- GCO
    1. docker pull gcr.io/google.com/cloudsdktool/cloud-sdk:latest
    2. docs: https://cloud.google.com/sdk/docs/downloads-docker

Provider Templates

- AWS
    1. Cloudformation
- GCP
- Azure
- Openstack
    1. https://docs.openstack.org/heat-dashboard/latest/user/template_generator.html

Provider Repositories
- AWS: 
    1. https://aws.amazon.com/codecommit
    2. git-codecommit.<REGION>.amazonaws.com
- AZURE
    1. devops Repositories
    2. 

