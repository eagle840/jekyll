---
layout: post
title: "Database Delivery"
permalink: /database/
date: 2023-20-02
---


# database delivery

## Gov.uk
- <https://www.gov.uk/government/groups/data-standards-authority>
- <https://dataingovernment.blog.gov.uk/2020/12/11/our-new-guidance-on-how-to-manage-data/>


### Database Schema Change challenges

1. Schema changes cannot delete data (in production)
2. In integration db's, teams need to coorindate changes
3. Keeping consistency across multiple envoriments  
4. During development, keeping track of changes to the database/merging conflicts

* Integration DB: A database used by multiple teams/projects. These types tend to get: cemented (data structure stuck), accumulutes technical debt, boilerplate apps(makes db less flexible)

2 ways of migrating these problems, using: Stated based,   Migration based.

Generally:
1. Keep it in a SCM, including all changes, schema, reference data (vs master data)
2. Use Continous Integration, inc tests
3. DB developers shouldn't use a shared DB, but use there own and use SCM to merge in changes
4. Refactor your database frequently, eg make sure tables/columns are all correctly named

* Master data: eg customer data, app has a way to change this  Reference Data: eg zip codes, country codes, apps don't change

### State based  (RE DO:   2:5)


??? you need to check following statement
Uses an 'Etalon' (snaphot like  declarative statement for the database). 
You can then run cmds against the DB to generate ?etalon?s that compare the db to the orginal snapshot and generate upgrade scripts

### Migration based

NOTES: Red 2:6&7

