# 9 FREE SOFTWARES FOR CYBERSECURITY ENTHUSIASTS

1. Operating System - [Kali Linux](https://www.kali.org/)
2. Email Security - [Deshashed](https://www.deshashed.com/)
3. Web Hacking - [Burp Suite](https://portswigger.net/burp)
4. Port Scan - [Nmap](https://nmap.org/)
5. Training - [HackTheBox](https://www.hackthebox.eu/)
6. Data Modification - [Cyber Chef](https://gchq.github.io/CyberChef/)
7. Intrusion Detection System - [Snort](https://www.snort.org/)
8. Firewall/Router - [PfSense](https://www.pfsense.org/)
9. Debugging - [Ghidra](https://ghidra-sre.org/)