# Python Cheat Sheet



## Install

### Linux

### Windows

Use the install from python.org (do not use windows store)

Installing from python.org gives you the **py** command, allowing control of multiple versions of python.

Launcher arguments:
-h     : print this help message and exit (also -? or --help)
-2     : Launch the latest Python 2.x version
-3     : Launch the latest Python 3.x version
-X.Y   : Launch the specified Python version
-0  --list       : List the available pythons
-V     : print the Python version number and exit (also --version)

See https://docs.python.org/using/windows.html#python-launcher-for-windows for
additional configuration.

Also see https://learn.microsoft.com/en-us/windows/python/beginners

Use the 'python' command, not 'python3'


#### Virtual environments

The `py` command in Windows does support virtual environments (venv). You can use it to create and manage virtual environments easily. Here's how you can do it:

1. **Create a Virtual Environment**:
   ```sh
   py -m venv myenv
   ```
   This command creates a virtual environment named `myenv`.

2. **Activate the Virtual Environment**:
   - **Command Prompt**:
     ```sh
     myenv\Scripts\activate
     ```
   - **PowerShell**:
     ```sh
     .\myenv\Scripts\Activate.ps1
     ```

3. **Deactivate the Virtual Environment**:
   ```sh
   deactivate
   ```

Using `py` with `venv` helps you manage dependencies and isolate your projects effectively¹(https://stackoverflow.com/questions/46896093/how-to-activate-virtual-environment-from-windows-10-command-prompt)²(https://python.land/virtual-environments/virtualenv).

Is there a specific project you're working on that you need help with?

Source: Conversation with Copilot, 05/03/2025
(1) python - How to activate virtual environment from Windows 10 command .... https://stackoverflow.com/questions/46896093/how-to-activate-virtual-environment-from-windows-10-command-prompt.
(2) Python venv: How To Create, Activate, Deactivate, And Delete. https://python.land/virtual-environments/virtualenv.

**Python to Windows executable**

| Tool       | Pros                                      | Cons                                      | URL                                |
|------------|-------------------------------------------|-------------------------------------------|------------------------------------|
| PyInstaller| - Easy to use                            | - Larger executable size                 | [PyInstaller](https://www.pyinstaller.org/) |
| cx_Freeze  | - Cross-platform support                  | - Limited customization options          | [cx_Freeze](https://cx-freeze.readthedocs.io/en/latest/) |
| py2exe     | - Specifically designed for Windows       | - Limited support for newer Python versions | [py2exe](http://www.py2exe.org/) |
| Nuitka     | - Optimizes and compiles Python code      | - More complex setup and configuration    | [Nuitka](https://nuitka.net/) |


**GUI Frameworks for Python on Windows**



| Framework   | Pros                                      | Cons                                                                                   | Software Link                                      |
|-------------|-------------------------------------------|----------------------------------------------------------------------------------------|----------------------------------------------------|
| Tkinter     | - Standard GUI toolkit in Python         | - Limited set of widgets                                                               | [Tkinter](https://wiki.python.org/moin/TkInter)    |
| Kivy        | - Cross-platform support                  | - Steeper learning curve                                                               | [Kivy](https://kivy.org/)                          |
| wxPython    | - Native look and feel on different platforms | - Documentation can be lacking                                                    | [wxPython](https://www.wxpython.org/)             |
| PyGTK       | - Native look and feel on Linux systems   | - Limited support for Windows and macOS platforms                                      | [PyGTK](https://pygtk.org/)                       |
| PySide (Qt) | - Powerful and feature-rich framework     | - Steeper learning curve compared to other frameworks                                | [PySide (Qt)](https://wiki.qt.io/PySide)          |

## Virtual Environments

### venv

https://docs.python.org/3/library/venv.html

### conda

### poetry

## Debugging

https://learn.microsoft.com/en-us/windows/python/beginners#working-with-python-in-vs-code
