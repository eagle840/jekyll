# Using Azure Alerts (in monitor)

## Diagram

flowchart TD
 subgraph s1["Target_Scopes"]
        n11["scope"]
        n12["type"]
        n13["signal_type"]
        n14["status"]
  end
 subgraph s2["Signals"]
        n21["various"]
  end
 subgraph s3["Conditions"]
        n4["Untitled Node"]
  end
 subgraph s4["suppression"]
        n22["Untitled Node"]
  end
 subgraph s5["Alerts"]
        n51["Severity"]
        n52["Monitored_service"]
        n53["Alert_condition"]
        n54["User_response"]
  end
 subgraph s6["Action_groups"]
        n55["Untitled Node"]
  end
 subgraph s8["General"]
        n56["RULE"]
        n57["Alert-processing"]
        n58["Action-group"]
  end
    s1 --> s2
    s2 --> s3
    s3 --> s4
    s4 --> s5
    s5 --> s6
    s6 --> s7["s7"]

    style n57 stroke:#AA00FF
    style n58 stroke:#00C853
    style s4 stroke:#AA00FF
    style s6 stroke:#00C853
    style s8 stroke:#D50000


## Health alerts


- It's in it's own portal. See 'Help + Support'
- see Setting up Service Health Alerts with John Savill
- When reviewing, look for **Action Requires**
- You can find custom created alerts in Monitor > Alerts > Alert Rules. filter for signal type is 'Service Health'

