# Azure SQL DB Restore

**NOTE:**
You cannot restore directly into an exsisting database

## START

Required:
- the sql Admin And PW (hopefully in a key vault)
- Know the pricing tier of the database (size and type)
- The correct name of the database (write it down)
- a bacpac file in blob store (see below) in a storage account
 

## Create a bacpac file for a backup
- some admins will have created a pipeline for this

Open a powershell prompt in Azure shell

In this example:
RG: deleteMe
Storeage Acc: nbstracc with a 'bacpacs' container
SQL server: nbsqlsvr
SQL db: nbdb
SQL db: --admin-user "adminuser" --admin-password "example"


```
# Define variables for resource group, storage account name, SQL server name, and database name
$resourceGroup = "deleteMe"
$storageAccountName = "nbstracc"
$sqlServerName = "nbsqlsvr"
$databaseName = "nbdb"
$adminUser = "admin-user"
$adminPassword = "example"

# Set output variables - first the account key of the storage account we will be exporting the back-up to
$storageKeys = az storage account keys list --account-name $storageAccountName --resource-group $resourceGroup
$storageAccountKeys = $storageKeys | ConvertFrom-Json
$storageKey = $storageAccountKeys[0].value

# Next, the name of the bacpac we'll be creating.
$dateStamp = Get-Date -Format "yyyy-MM-dd-HH-mm-ss"
$bacpacName = "production-backup-$($dateStamp).bacpac"

# allow access rules
az storage account update --resource-group $resourceGroup --name $storageAccountName --default-action Allow
az sql server firewall-rule create --name AllowAllWindowsAzureIps --resource-group $resourceGroup --server $sqlServerName --start-ip-address 0.0.0.0 --end-ip-address 0.0.0.0

# backup database
az sql db export --admin-user $adminUser --admin-password $adminPassword --storage-key "$($storageKey)" --storage-key-type StorageAccessKey --storage-uri "https://$storageAccountName.blob.core.windows.net/bacpacs/$($bacpacName)" --name $databaseName --resource-group $resourceGroup --server $sqlServerName

# remove access rules, remember this cuts off access
az sql server firewall-rule delete --name AllowAllWindowsAzureIps --resource-group $resourceGroup --server $sqlServerName
az storage account update --resource-group $resourceGroup --name $storageAccountName --default-action Deny
```


Notes:
- Export command docs: https://learn.microsoft.com/en-us/cli/azure/sql/db?view=azure-cli-latest#az-sql-db-export
- 0.0.0.0 is 'all Azure-internal IP addresses' [docs](https://learn.microsoft.com/en-us/cli/azure/sql/server/firewall-rule?view=azure-cli-latest#az-sql-server-firewall-rule-create-optional-parameters)

## Delete the OLD BD

**!CAUTION** - you may want to restore the db to a temp db (name) to confirm that the data is valid, once confirmed can may delete the orginal database. (check with your team for a good sql query to confirm data is upto date and valid)
- In the SQL database (overview) delete the resource = NOT the server

## Restore the DB

In the SQL server blade (overview), select `Import Database`
- if prompted at top of page 'Allow Access'
- select the created bacpac from 'storage'
- Select the pricing tier (that you noted in step one)
- Enter the database name
  - you can't overwrite an exisising db
  - you if need to confirm the data, before deleting the orginal database, use different name from the db you're replacing.
  - if you deleted the orginal db, but the orginal name in.
- Set AuthN to SQL
- Enter UN and PQ
Select  'OK' (if you can't check top of page for 'Allow Access')
You and now monitor the SQL server as below.
- Note that the connection strings will not change when using the orginal DB name.


## monitor

In the SQL server blade, select the `Import/Export history` blade, you can change the status for backups/restores.

## Remove the test db  (if needed)

 - In the SQL database (overview) delete the resource.

 ## Resouces

 - https://stackoverflow.com/questions/58097139/restore-azure-sql-db-over-an-existing-db-to-maintain-backup-history/58101933#58101933
 - https://learn.microsoft.com/en-us/answers/questions/1372872/how-to-restore-azure-sql-database-without-losing-b


