# MS Defender for Cloud

[MS Docs](https://learn.microsoft.com/en-gb/azure/defender-for-cloud/)

Main Marketed features:
- Security posture monitoring
- Regulatory compliance
- Attack path analysis
- Cloud workload protection
- Vulnerability scanning
- DevOps posture visibility
- Infrastructure-as-code security
- Code security remediation guidance


(CSPM) provides detailed visibility into the security state of your assets and workloads, and provides hardening guidance to help you efficiently and effectively improve your security posture.

cloud workload protection platform (CWPP) integrated within Defender for Cloud for advanced, intelligent protection of your workloads running on Azure, on-premises machines, or other cloud providers


# Free and paid options:

## Foundation CSPM (on all resources) 
Continuous assessment of the security configuration of your cloud resources
Security recommendations to fix misconfigurations and weaknesses
Secure score summarizing your current security situation



## Defender CSPM ($5)
Identity and role assignments discovery
Network exposure detection
Attack path analysis
Cloud security explorer for risk hunting
Agentless vulnerability scanning
Agentless secrets scanning
Governance rules to drive timely remediation and accountability
Regulatory compliance and industry best practices
Data-aware security posture
Agentless discovery for Kubernetes
Agentless container vulnerability assessment

## Cloud Workload Protection
comprehensive
- Servers
- App Service
- Databases
- Storage
- Containers
- Key Vault
- Resouce Manager
- API

# Turning on

Goto MS defender for cloud > **Environmental Settings**
- Select Subscription
  - Select
     - CSPM
       - Foundational (on by default), free
       - Defender CSPM ($5 resource)
     - CWP Plans (off)
       - Servers
       - App Service
       - Databases
       - Storage
       - Containers
       - Key Vault
       - Resouce Manager
       - API
