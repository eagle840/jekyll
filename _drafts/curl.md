# curl command

In windows use a standard command prompt. PS is it's own thing and doesn't work like curl

By Daniel Stenberg (1998)

Over 230+ options


## Basic

curl www.example.com  # GET with output to screen

-i get back headers
-I header only (no body)
-L follow redirects
-I -L  show orginal and redirect
-v verbose
--trace
--trace-ascii
-o save
-H add a header -H "name:nick"
  - -H "User-agent:" # removes header
  - -H "User-agent;" # blank header

## Post & PUT

Void using -X POST

-d  POST -d name=nick
-d @file
-d @-  # stdin
--data-binding @file.json -H "Content-type: application/json"

-T # is put

## URL Globby

curl www.example.com/[1-9].html  #returns 1.html, 2.html etc

-o save_#1.html 


## Cookies

'$ curl -c cookiejar.txt https://example.com'  saves cookies in cookiejar.txt

'$ curl -b cookiejar.txt https://example.com'  sends cookies in cookiejar.txt

## Cookies in a login

'''
$ curl -c cookiejar.txt https://example.com/login_form

$ curl -b cookiejar.txt -c cookiejar.txt https://example.com/login -d user=daniel
-d password=1234

$ curl -b cookiejar.txt -c cookiejar.txt https://example.com/profile
'''

## HTTPS

-k ignore cert checks  # AVOID rough way of doing it
-k https://127.0.0.1/ -H "Host:example.com" # works with cookies, but not virtual servers
-k https://example.com/ --resolve example.com:443:127.0.0.0 # forces example.com on 443 to resolve to 127.0.0.1 (443? or 80?)
  - does TLS & certs correctly, works with cookies, works with virtual servers
-k https://example.com/ --connect-to example.com:443:127.0.0.0:host.tld:8443

## More verbose

--trace-ascii - #include last dash?
--trace

## Extracting transfer meta-data

```
--write-out (-w) is your friend
--write-out 'text and %{variables} to see'
 - Variables include:
 - content_type
 - http_version
 - remote_ip
 - local_ip
 - speed_download
 - and many others
-o saved -w '%{json}' | jq  # for all of it
```

## Convert curl into an app

```
$ curl https://example.com/ --libcurl sourcecode.c

$ gcc sourcecode. c -lcurl -o ./myapp

$ ./myapp

$ cat sourcecode.c
```

## Mimic that browser

Most browers have a 'copy as curl' in the developer/networking tool

## SSLKEYLOGGER

see 36:45   https://www.youtube.com/watch?v=I6id1Y0YuNk

@ 40 mins  Need to continue making notes

