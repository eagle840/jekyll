---
layout: page
title: "Azure Oauth Unleashed: Taming the Identity Beast"
permalink: /AzureFunctions/
date: 2024-01-09
---


# Using Oauth on auth

Microsoft Identity Platform is OpenIC connect compliant [docs](https://learn.microsoft.com/en-us/entra/identity-platform/)

Getting started [MS Learn](https://learn.microsoft.com/en-us/training/modules/getting-started-identity/)

To get started, create App Reg, and then goto the Quickstart blade, it will provide many programming examples.

## Libraries

- MSAL [docs](https://learn.microsoft.com/en-us/entra/identity-platform/msal-overview), [link](https://learn.microsoft.com/en-us/azure/active-directory/develop/msal-client-applications)
- Microsoft.Identity.Web for .Net [docs](https://learn.microsoft.com/en-us/training/modules/getting-started-identity/) [example](https://github.com/Azure-Samples/active-directory-aspnetcore-webapp-openidconnect-v2/tree/master/1-WebApp-OIDC)

## Tokens (implicit grant flow)

- ID Tokens: includes claims about the user, used by OIDC. A web app will use this to tell who you are
- Access Tokens: used for AuthZ for use with resources, you should never touch the token, just pass it along. Used with API's
- Refresh Tokens: to get new tokens when old ones expire.
- Additional custom tokens can be added in the App Registration, in the tokens blade.


|                     | ID Token                                      | Access Token                                 |
|---------------------|-----------------------------------------------|----------------------------------------------|
| Purpose             | Used for authentication and user identification | Used for authorization and accessing resources |
| Content             | Contains user information and claims           | Contains authorization details and scopes     |
| Audience            | Intended for the client application            | Intended for the resource server              |
| Expiration          | Typically short-lived (e.g., few minutes)      | Can be long-lived (e.g., hours, days)         |
| Usage               | Used by client application to verify identity  | Used by client application to access resources|
| Claims              | Contains user-specific claims (e.g., sub, iss) | Contains authorization-specific claims        |
| Authentication Level| May include authentication context information| Does not include authentication context       |
| Scope               | Does not have a concept of scope               | Contains scope(s) for resource access         |
| Token Type          | JWT (JSON Web Token)                           | Can be JWT, OAuth token, or other formats     |

## JWTs

API endpoint url/api/v1.0/examplex
- POST JSON credentials (return JWT?)  {"username": "userX", "password": "mypswd"}
- GET prev escalution and .... CHECK THM  "authunication Bear <insert token>"

JWT = Header (type +  sign algorthym) + payload (claims) + signnituee

signitnuew types: 
 - none, (reconstructing the JWT with none is a way to brake it)
 - symmeteric (eg HS265, and you'll need the key)
 - Asym (eg RS256)



### Learn

- Microsft https://learn.microsoft.com/en-us/training/paths/m365-identity-associate/
    This includes links to screen casks and github repos. You'll see them in the intro section of each module

#### Videos
- Detailed AuthZ code flow on Azure [concept works](https://www.youtube.com/watch?v=pfBc2EIbgQw)