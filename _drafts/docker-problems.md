---
layout: page
title: Docker Cheatsheet
permalink: /docker/
---

# registries-repos-images-and-tags

So you've created your docker image masterpiece and ready to spread it to the world.
You'll need to know the difference between these 4 components first, we'll start with tags and images.

## Tags and images.

First off, tag is not what you think it is. When I first saw the name I was thinking - 'metadata'. It's not.
In my opinion I consider it the name of the image! Yeah i know, that's completely different from what the term tag means.
In the rest of the document, i'll be using tag as meaning name.

Well what if I build an image without a tag added? - It'll be given the tag name 'latest', (over-writting the one if you already have one - you'll see the change in the ImageID)

So what is the image name?

If we do a [sudo] 'docker images' you'll see the header actually say Repository.  IE - there is no image name. it's the repo name. (it's the imageID that actually points to the phyical image)

Now slashes have a specific meaning in the repository column, and we'll take a closer look at them under the section: docker push, below.


## Registries and Repo's

A registry is an online (or local if you install one) service that lets you push (or pull) an image up to it.

Repo's, you create a repo in  the registry (when signed up) to store your images. this repo can be under your userID or under an organiztion-name if you create and/or have access to one.  You can make this repo private or public. (Docker hub give you a single free repo)

ie: a registry has users, who have repos, that they can store images in - which are called tags.

## Docker push

Lets take a look at a couple of simple push commands*:

'docker push joe/myrepo:v1'  

'docker push myregisty.com/joe/myrepo:v1'

In the first command you'll see  it doesn't have a dns name in the repositry name, that's because it's using the  default which is docker's registry at docker.io 

Now the thing you have to do before pushing an image, is to make sure that it's repo name matchs the destination using the command 'docker tag'.
What it will do is to create another entry in your images list that points to the orginal repo:tag (remember these repos point to an imageID)

docker tag [orginalrepoName]:[tag] [newreponame]:[tag]  # remember, if no tag name, it will use 'latest'

and now you'll be ready to push that image upto the registy/repo



*(assuming you have already 'docker login' to your registry account)




## Docker login

Just a quick note on docker login:

When you 'docker login', docker stores the login info in  ~/.docker/config.json file.
which includes the registry name and the login info, be aware the password is not encrypted, just base64.




## The short version


Standard repo format:
[registryDNS][:port]/[userId|organization]/[repoName]:[tag|latest]


- docker login -u username myregistey.com
- use docker tag to copy an *image*, and set it to a repo name that makes the repo name equal to:  registry/repo:tagname
   eg: docker tag myimage:v1  myregisty.com/username/repo:tag
- push that image to the repo: docker push myregisty.com/username/repo:tag
- or push all the tags(images) in that repo  with: docker push myregisty.com/username/repo

## DOcker build exit codes
