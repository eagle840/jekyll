# Azure ACI

Docs: https://learn.microsoft.com/en-us/azure/container-instances/

While the portal offers minimal setup, and az cmd offers more. Suppling a yaml is the best option:

`az container create --resource-group myResourceGroup --file secure-env.yaml \`

typical yaml:

```yaml
apiVersion: '2019-12-01'
location: eastus
name: file-share-demo
properties:
  containers:
  - name: hellofiles
    properties:
      environmentVariables: []
      image: mcr.microsoft.com/azuredocs/aci-hellofiles
      ports:
      - port: 80
      resources:
        requests:
          cpu: 1.0
          memoryInGB: 1.5
      volumeMounts:
      - mountPath: /aci/logs/
        name: filesharevolume
  osType: Linux
  restartPolicy: Always
  ipAddress:
    type: Public
    ports:
      - port: 80
    dnsNameLabel: aci-demo
  volumes:
  - name: filesharevolume
    azureFile:
      sharename: acishare
      storageAccountName: <Storage account name>
      storageAccountKey: <Storage account key>
tags: {}
type: Microsoft.ContainerInstance/containerGroups
```

For a quick overview, see https://learn.microsoft.com/en-us/training/modules/create-run-container-images-azure-container-instances/6-mount-azure-file-share-azure-container-instances

## az cmd

### Create an container:

`az container create --resource-group myResourceGroup --name aci-tutorial-app --image <acrLoginServer>/aci-tutorial-app:v1 --cpu 1 --memory 1 --registry-login-server <acrLoginServer> --registry-username <service-principal-ID> --registry-password <service-principal-password> --ip-address Public --dns-name-label <aciDnsLabel> --ports 80`

### Pull logs with cmd

`az container logs --resource-group myResourceGroup --name aci-tutorial-app`

## Restart polices

| Restart policy | Description |
|----------------|-------------|
|Always |	Containers in the container group are always restarted. This is the **default** setting applied when no restart policy is specified at container creation. |
|Never |	Containers in the container group are never restarted. The containers run at most once.|
|OnFailure |	Containers in the container group are restarted only when the process executed in the container fails (when it terminates with a nonzero exit code). The containers are run at least once.|

## Container Groups

The top-level resource in Azure Container Instances is the container group. A container group is a collection of containers that get scheduled on the same host machine. The containers in a container group share a lifecycle, resources, local network, and storage volumes. It's similar in concept to a pod in Kubernetes.

Note that only one IP port is exposed in the group, and from the one container



