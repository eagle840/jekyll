

# C#

https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/program-structure/

**NOTE** there appears to be two main docs, [docs](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/program-structure/)  and [ref](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/language-versioning)  and [api](https://learn.microsoft.com/en-us/dotnet/api/system.object?view=net-8.0)


## notes

- Single Main method: there can (should) be only one Main method, if there are multiple you have to use sepecify it at compiler time [link](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/advanced#mainentrypoint-or-startupobject)
- stack vs heap: The stack is used for static memory allocation with faster access speed and automatic memory management, while the heap is used for dynamic memory allocation with slower access speed and manual memory management. The stack has a limited size and follows a last-in-first-out (LIFO) order, while the heap has a larger size and allows for flexible memory allocation and deallocation.


## Types [docs](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/types/)

- built-in/Predefined [link](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/built-in-types)
  - primetive: bool, int, char etc
  - other: sbyte, ubyte, object, string
- custom