# UUID's


- GUID = UUID
- See below for ULID
- 128bits long =
- 32 hex digits grouped into chunks of 8-4-4-4-12
- about 10^38 numbers
- https://www.guidgenerator.com/online-guid-generator.aspx
- rfc 4122
- https://en.wikipedia.org/wiki/Universally_unique_identifier

1. General types;
    - Random
    - Time based
    - HW based (eg MAC addrress)
    - Content based (hash of data)
    - Mixture of above
    
2. Uses:
    - db keys
    - filenames
    - resource names
    - ID's
    
3. Pros:
    - No central authority
    - Easily combined
    
4. Cons:
    - Appear random
    - Space
    - V.Very small chance of collision


# ULID's

https://json-server.dev/ulid-vs-uuid/

- time stamp sortable