---
layout: post
title: "Installing Linux on Windows 10 using WSL2"
---

#  An intergrated Linux solution on Windows.

For a long time now users have been able to run Linux through a VM on Windows, and then with the intergration of WSL it provided to be a smoother experience, however Mircosoft has made a few changes so that now runs virtualizes - WSL2, signifcately speeding up it execution. 

In this blog I'll be covering the installation to get it up and running.

1. confirm and turn on Virualization in the BIOS
2. update your version on Windows 10 (it can be home edition) to 2004 (run winver)
3. Run 'Turn Windows features on or off' and turn on: Virtial Machine Platform  and Windows Subsystem for Linux
4. Goto the Windows Store and install/launch Ubuntu 20.04 (or your perferred version)
5. Download/install the Kernel Updrade at https://aka.ms/wsl2kernel
6. Open a powershell prompt and run: 'wsl -l -v'  
7. run wsl --set-version Ubuntu-20.04 2  # to set the Kernel to verion 2
8. run wsl --set-default-version 2  # to make version 2 every time
(on my system I had to remove/restart/install 'Virtial Machine Platform' again to set the kernel version to 2)
9. Type ubuntu in the search bar and it should run.


For the advanced settings, see https://learn.microsoft.com/en-us/windows/wsl/wsl-config


