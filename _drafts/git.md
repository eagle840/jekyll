---
layout: page
title: "Git be up to go go"
permalink: /git/
date: 2024-07-14
---

filename 2024-07-14-Git.md


Main GitHub Docs: https://docs.github.com/en

Pro Git (free online book from the makers of Git) https://git-scm.com/book/en/v2

Interactive git learn https://learngitbranching.js.org/

# Git and Github Cheetsheet

- git log --graph --oneline --all --decorate
- git status
- git branch (new branch new)
- git branch -av ; list all branchs
- git checkout (name to branch to goto)
- git merge (branchname); merge the named branch into the one you're in now
- git cherry-pick c1 c3 c7 ; run selected commited against your working commit
- git rebase [-i] branch_to_rebase_to branch_to_take_commits_from # runs commits against branch from the base commit of the two 
- git tag tag_name commit_hash # tags are stuck to a commit, if no commit, it will do where the head is

- Important
  - The way Git works, files are not renames, but deleted and recreated. So Do Not edit the contents. If you have to, make sure to do one, the commit, then do the other.

## Moving branchs around

- git branch -f main [HEAD^|branch|commit] # moving branches around, forces main to a different place, not allowed on the current branch
- git reset HEAD~2 # take present branch forward two (should be local only)
- git revert HEAD^^ # creates a new commit based on copy of HEAD^^ (for shared repos)

## terms

- HEAD;  points to the working commit (can be a commit or a branch)
- DETACHED HEAD; detached HEAD means points to a commit, not a branch, `git checkout *commt-hash*`
- Rebase <into branch name>; ? running commits from one branch into another (and landing in that branch), dangerous with other shared work
- merge <branch name>; merging two branches with the same common base commit
- git squash; not a command, but the process of merging multiple commits into one.
- relative commits
  - Moving upwards one commit at a time with ^, git checkout main^^ ie two commits up
  - Moving upwards a number of times with ~<num>

## Tagging

- https://git-scm.com/book/en/v2/Git-Basics-Tagging
- referene to a commit
- lightweight: a checksum stored in a file `git tag v1.4` 
- Annotated: A object in git. `git tag -a v1.4 -m "my version 1.4"`
- git push DOES NOT push tags. use `git push origin v1.4`

## Releases

- needs a tag (new or old)

## Compare

- to compare in github, add `/compare` to the end of the url

### Basic Workflow

### Feature Branch Workflow

### Git Flow 

### Gitlab Flow


# tools

- Gitlens VSC extension

## Github Actions

Links

- github.com/marketplace
- contexts: https://docs.github.com/en/actions/learn-github-actions/contexts
- https://github-actions-hero.vercel.app/  # interactive and lessons

## Freq Used GH Actions:

- https://github.com/tj-actions/changed-files

