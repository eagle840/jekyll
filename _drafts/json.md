# JSON

## JSON Validation

**The JSON website** https://www.json.org/json-en.html

## JSON Schema

A way to valid a json file

Helps with
- formatting
- syntax
- data types
- structure & content

[Search the JSON Schema Store](https://www.schemastore.org/json/)

- https://github.com/jsonsystems/public   [docs](https://jsonschema.net/)


Includes a list of validators for different languagaes [link](https://json-schema.org/implementations)
- check the tools section of this website for more tools 

Lessons:

- [An Introduction to JSON Schema: A Practical Look](https://www.youtube.com/watch?v=dtLl37W68g8)

### Validator

- https://jsonschema.dev/
- https://www.jsonschemavalidator.net/

There are also tools that can take a json file, and create a schema from it.


## JSON language support


Many languages use json object, but have to transmit and recieve these json objects as strings. Produce a markdown table the shows the major langiuages. Include columns: language, to string, from string


| Language | To String | From String |
|----------|-----------|-------------|
| JavaScript | `JSON.stringify(obj)` | `JSON.parse(str)` |
| Python | `json.dumps(obj)` | `json.loads(str)` |
| Java | `new Gson().toJson(obj)` | `new Gson().fromJson(str, type)` |
| C# | `JsonConvert.SerializeObject(obj)` | `JsonConvert.DeserializeObject<T>(str)` |
| Ruby | `obj.to_json` | `JSON.parse(str)` |
| PHP | `json_encode(obj)` | `json_decode(str)` |
| Go | `json.Marshal(obj)` | `json.Unmarshal(data, &obj)` |
| Swift | `try JSONEncoder().encode(obj)` | `try JSONDecoder().decode(type, from: data)` |
| Kotlin | `Gson().toJson(obj)` | `Gson().fromJson(str, type)` |
| Rust | `serde_json::to_string(&obj).unwrap()` | `serde_json::from_str(&str).unwrap()` |
| C++ | `nlohmann::json::to_string(obj)` | `nlohmann::json::parse(str)` |
| TypeScript | `JSON.stringify(obj)` | `JSON.parse(str)` |
| Perl | `encode_json(obj)` | `decode_json(str)` |
| Swift | `try JSONEncoder().encode(obj)` | `try JSONDecoder().decode(type, from: data)` |
| Kotlin | `Gson().toJson(obj)` | `Gson().fromJson(str, type)` |
| Rust | `serde_json::to_string(&obj).unwrap()` | `serde_json::from_str(&str).unwrap()` |
| C++ | `nlohmann::json::to_string(obj)` | `nlohmann::json::parse(str)` |
| TypeScript | `JSON.stringify(obj)` | `JSON.parse(str)` |
| Perl | `encode_json(obj)` | `decode_json(str)` |
| PowerShell | `ConvertTo-Json -InputObject $obj`        | `ConvertFrom-Json -InputObject $str`     |

(not confirmed)

## JSON Path

- a JSON path tool for npm, but more important, a list of JSONPATH syntax/query [link](https://github.com/dchester/jsonpath#jsonpath)

## other

- https://learn.microsoft.com/en-us/azure/azure-monitor/alerts/alerts-common-schema
- ocsf Open Cybersecurity Schema Framework https://github.com/ocsf
- Free KodeKloud course on JSON - https://kodekloud.com/courses/json-path-quiz/
- arrays(list) vs objects(KV pairs) - https://stackoverflow.com/questions/5293555/how-do-you-represent-a-json-array-of-strings