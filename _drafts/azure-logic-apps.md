# Azure logic Apps


https://learn.microsoft.com/en-us/azure/logic-apps/

**The New Designer** for Azure Logic Apps [MS Community link](https://techcommunity.microsoft.com/t5/azure-integration-services-blog/introducing-the-new-azure-logic-apps-designer-for-consumption/ba-p/4095261)

## Components

- connectors, operations, custom: https://learn.microsoft.com/en-gb/training/modules/intro-to-logic-apps/3-how-logic-apps-works
- custom connectors use OPEN API specs (swagger)
- support AS2 / X12 / EDIFACT
- Connectors for MQ and SAP


## HTTP triggers
When triggering an HTTP Azure Logic App, the query parameters `sp`, `sv`, and `sig` are part of the Shared Access Signature (SAS) used to secure the endpoint. Here's what each parameter represents:

- **`sp` (Permissions)**: Specifies permissions for the allowed HTTP methods to use.
- **`sv` (Service Version)**: Specifies the SAS version to use for generating the signature.
- **`sig` (Signature)**: Specifies the signature to use for authenticating access to the trigger. This signature is generated by using the SHA256 algorithm with a secret access key on all the URL paths and properties. This key is kept encrypted, stored with the logic app, and is never exposed or published. Your logic app authorizes only triggers with a valid signature created with the secret key.

These parameters help secure your Logic App by ensuring that only authorized requests can trigger it. If you have any more questions or need further details, feel free to ask!

Source: Conversation with Copilot, 23/09/2024
(1) Logic App Best practices: #19 Secure your Azure Logic Apps - Turbo360. https://turbo360.com/blog/logic-app-best-practices-secure-your-azure-logic-apps.
(2) Create parameters for workflow inputs - Azure Logic Apps. https://learn.microsoft.com/en-us/azure/logic-apps/create-parameters-workflows.
(3) How To: HTTP Request Trigger Picks Up Request Paramaters From JSON .... https://learn.microsoft.com/en-us/answers/questions/725869/how-to-http-request-trigger-picks-up-request-param.
(4) Invoke Azure LogicApp over HTTPS with HTTP triggers - cmatskas. https://cmatskas.com/invoke-azure-logicapp-over-https-with-http-triggers/.
(5) undefined. https://prod-05.uksouth.logic.azure.com:443/workflows/6d598e2263844ace88072f3ed72bf7eb/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=XXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.

## Webhook testing

- https://pipedream.com/requestbin   **click on Create a public bin instead.**
  - Set URI as lised
  - Method: POST ?
  - Headers: Content-Type: application/json
  - body
    ```
    {
  "channel": "#funding-alerts",
  "username": "My-WebhookBot",
  "text": "this is a test",
  "icon_emoji": ":borat:"
}
    ```

## mock outputs

- https://learn.microsoft.com/en-us/azure/logic-apps/test-logic-apps-mock-data-static-results?tabs=consumption

## using results

WIP: 
- create a query, and set the 'data source' as 'merge' and set as 'Duplicate table', using the step name (in adv settings) of the query
- not confirmed, but suggestion of use:   `MyStepName | project TableInStep`
- https://github.com/microsoft/Application-Insights-Workbooks/blob/master/Documentation/Samples/ReusingQueryData.md

## Simple http response

- Create a 'when a HTTP request is received'
- add 'response'
  - see https://learn.microsoft.com/en-us/azure/connectors/connectors-native-reqres?tabs=consumption#add-a-response-action

# Notes

- When doing curl request, put the url in ""'s
- While 'All methods' is set in the app, it appears only -X POST in curl works
- Parameters can only be in the url query according to this [blog](https://learn.microsoft.com/en-us/answers/questions/725869/how-to-http-request-trigger-picks-up-request-param)
- triggering of HTTP REST request with ARM [REST](https://learn.microsoft.com/en-us/rest/api/logic/workflow-triggers/run?view=rest-logic-2016-06-01&tabs=HTTP)
- tips and tricks [link](https://turbo360.com/blog/logic-app-best-practices-secure-your-azure-logic-apps)


# Links

- MS blogs on Azure Logic Apps [link](https://techcommunity.microsoft.com/t5/azure-integration-services-blog/bg-p/IntegrationsonAzureBlog/label-name/Logic%20Apps)
- Logic Apps Day 2024, a varity of topics [link](https://developer.microsoft.com/en-us/reactor/events/23570/)

# Alternatives

- power-automate
- n8n
- apache camel
- node-red (runs on embeded items as well)
- make.com $$
- zapier $$