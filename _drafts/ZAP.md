# OWASP ZAP





## Integrating AJAX spider

sudo apt install libjenkins-htmlunit-core-js-java

## CI/CD

To install zap2docker, you can pull it from Docker Hub using the following command:

`docker pull owasp/zap2docker-stable`
You can run ZAP from the docker instance using one of the packaged scan scripts, which will allow you to run one of the following scan profiles:

Baseline Scan: ZAP will spider the target website for a maximum time of 1 minute. No active scan will be performed. You can optionally run an AJAX spider if desired.

`docker run -t owasp/zap2docker-stable zap-baseline.py -t https://www.example.com`
Full Scan: ZAP will run a spider with no time limits, followed by an active scan.

`docker run -t owasp/zap2docker-stable zap-full-scan.py -t https://www.example.com`
API Scan: ZAP will perform an active scan against an API. You will need to provide a URL to an API description file (OpenAPI, GraphQL or SOAP).

`docker run -t owasp/zap2docker-stable zap-api-scan.py -t https://www.example.com/swagger.json -f openapi`
In any of the scans, the results for passive scans will be limited to 10 alerts. In addition to the base commands, you can add the -j switch to perform an AJAX scan on the baseline and full scans.


## example api jenkins yaml file

```
node{

  git branch: "main", url: "http://172.17.0.1:3000/thm/simple-api"

  stage ('Build the Docker image') {
    sh "echo building the image..."
    sh "docker build --tag simple-api:latest ."
    sh "echo building image complete."

  }

  stage ('Deploy the Docker image') {
    sh "echo Deploying the container..."
    sh " docker rm -f simple-api"
    sh "docker run -d -p 8081:5000 --name simple-api simple-api:latest "
    sh "echo Container successfully deployed."

  }


  stage ('Scan with OWASP ZAP') {
    sh "mkdir -p zap-reports"
    sh "chmod 777 zap-reports"
    sh "docker run -v \$(pwd)/zap-reports:/zap/wrk/:rw -t owasp/zap2docker-stable zap-api-scan.py -t http://172.17.0.1:8081/swagger.json -f openapi -r api-simple-api-${env.BUILD_NUMBER}.html"
  }

}

```

## simple web app scan

```
  stage ('Scan with OWASP ZAP') {
    sh "mkdir -p zap-reports"
    sh "chmod 777 zap-reports"
    sh "docker run -v \$(pwd)/zap-reports:/zap/wrk/:rw -t owasp/zap2docker-stable zap-baseline.py -t http://172.17.0.1:8082/ -r baseline-simple-webapp-${env.BUILD_NUMBER}.html"
  }

```