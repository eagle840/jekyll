---
---

# Using Mermaid

Does Mermaid work with gitlab?, what about github?

* https://gitlab.com/gitlab-org/gitlab-foss/-/issues/3711
* https://mermaid-js.github.io/mermaid-live-editor # Switch to playground to drag and drop
* https://mermaid-js.github.io/mermaid/#/

## GUI tools

- https://www.mermaidchart.com/play
- https://mermaid.live/edit

## font awesome

https://fontawesome.com/icons

`C -->|Six| F[fa:fa-car Prod]`

Lets test it!


```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```

```mermaid
graph TD
A[Client] -->|https| B(WAF)
subgraph Vnet Internal
B --> C{Load Balancer}
end
subgraph Vnet Internl
  C -->|One| D[Dev]
  C -->|Two| E[Test]
  subgraph dedicated asp
    C-->|Three| G
    C-->|Four| G
    C-->|Five| G[PreProd]
  end
  subgraph dedicated asp
    C -->|Six| F[fa:fa-car Prod]
  end
end
```


A more complex diagram:

graph TD
 linkStyle default interpolate basis
 wan1[<center>DSL 100/10 Mb<br><br>10.100.102.1</center>]---router{<center>EdgeRouter-X<br><br>10.20.30.1</center>}
 ip((<center><br>IP<br><br></center>))-.-router
 dns((<center><br>DNS<br><br></center>))-.-router
 wan2[<center>LTE 50/20 Mb<br><br>192.168.1.1</center>]---router
 router---|100Mb|ap[<center>RT-AC1200<br><br>10.20.30.3</center>]
 router---|1Gb|pc(<center>PC<br><br>10.20.30.190</center>)
 router---|1Gb|switch[<center>TL-SG105E<br><br>10.20.30.2</center>]
 subgraph red1
 ap-.-cam1(<center>Camera<br><br>10.20.30.171</center>)
 ap-.-cam2(<center>Camera<br><br>10.20.30.172</center>)
 ap-.-phone(<center>Phone<br><br>10.20.30.191</center>)
 ap-.-ir(<center>IR<br><br>10.20.30.180</center>)
 end
 subgraph red2
 switch---|100Mb|pi1(<center>RPi 3B<br><br>10.20.30.150</center>)
 switch---|1Gb|pi2(<center>RPi 3B+<br><br>10.20.30.151</center>)
 switch---|100Mb|nvr(<center>NVR<br><br>10.20.30.170</center>)
 switch---|1Gb|laptop(<center>Laptop<br><br>10.20.30.192</center>)
 end

 ---

 flowchart TD
 subgraph s1["Target_scopes"]
        A1["scope target"]
        A2["Type"]
        A3["signal type"]
        A4["status"]
  end
 subgraph s2["Signals"]
        B1["Types"]
        B2["metrics"]
        B3["log"]
        B4["Activity"]
        B5["Smart Detector"]
  end
 subgraph s3["Conditions"]
        n4["Untitled <br />Node"]
  end
 subgraph s4["Subpression/None"]
  end
 subgraph s5["Alerts"]
        E1["servirty"]
        E2["Service"]
        E3["AppIn SIghts"]
        E4["alert contion: <br />Fired/Resolved"]
        E5["resource"]
        E6["user_response"]
        E7["Action Group triggered"]
  end
 subgraph s6["Action_groups"]
        F1["SMS/email"]
        F2["Automatioin"]
  end
    n1 --> n2
    s2 --> s1
    s3 --> s2
    s4 --> s3
    s5 --> s4
    s6 --> s5



