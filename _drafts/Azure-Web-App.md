---
layout: post
title: "Building a ASP webapp with Azure Web App Service"
---


Splitting this into 4 docks: 
- Apps and AAD
- Web Apps  (this doc)
- Api Apps
- Container Apps
- Function appa
- Troubleshooting Azure Apps

# Setting a SP with scope

az ad sp create-for-rbac --name "{sp-name}" --sdk-auth --role contributor --scopes /subscriptions/{subscription-id}

- you can set scopes at different levels, default is no role/scope
- MS docs [AZ AD SP](https://learn.microsoft.com/en-us/cli/azure/ad/sp?view=azure-cli-latest)

# Building a ASP webapp with Azure Web App Service

This blog will show creating an azure web app with out using Visual Studio.

- [Quickstart: Deploy an ASP.NET web app](https://learn.microsoft.com/en-us/azure/app-service/quickstart-dotnetcore?tabs=net70&pivots=development-environment-cli)
- [templates](https://learn.microsoft.com/en-us/dotnet/core/tools/dotnet-new-sdk-templates)
- determine version -> open *.csproj and look for `<TargetFramework>` element.
- add application insight [doc](https://learn.microsoft.com/en-us/azure/azure-monitor/app/asp-net-core?tabs=netcorenew%2Cnetcore6)


## Package deployment

- You'll see 'loaded package'
- A zip file, name (windows)web.config  (linux)startup
- check kudo logs for deployment of zip file

## Add Application insites

- `dotnet add package Microsoft.ApplicationInsights.AspNetCore`
- - add application insight [doc](https://learn.microsoft.com/en-us/azure/azure-monitor/app/asp-net-core?tabs=netcorenew%2Cnetcore6)
- add `builder.Services.AddApplicationInsightsTelemetry();` to program.cs
- add 
```json
  "ApplicationInsights": {
    "ConnectionString": "Copy connection string from Application Insights Resource Overview"
  }
``` to appsettings.json
