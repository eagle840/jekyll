# Azure SQL 

3 types

- Azure Sql Database
- Azure Managed SQL
- Azure VM SQL
- Azure SQL Edge [link]https://learn.microsoft.com/en-us/azure/azure-sql-edge/)

Azure SQL Docs  https://learn.microsoft.com/en-us/azure/azure-sql/?view=azuresql

Migration Guide (according to MS): https://learn.microsoft.com/en-us/data-migration/

Azure SQL Workshop: aka.ms/azuresqlworkshop



## Azure VM SQL

!For some reason, this will not deploy using the GUI interface, you'll need to use the [quickstart template](https://learn.microsoft.com/en-us/azure/azure-sql/virtual-machines/windows/create-sql-vm-resource-manager-template?view=azuresql&tabs=CLI)

- To move VM SQL into Azure BD, use SMSS Tasks>deploy to Azure DB [see this article](https://stackoverflow.com/questions/33436344/restoring-sql-server-backup-to-azure-sql-database)
- there is a service/extension that can be added to the VM, with will showup in the VM blade as SQL [link](https://learn.microsoft.com/en-us/azure/azure-sql/virtual-machines/windows/sql-server-iaas-agent-extension-automate-management?view=azuresql&tabs=azure-portal#limitations)
- One of the backup options backs up to Storage account.


## Tools

- [bcp](https://learn.microsoft.com/en-us/sql/tools/bcp-utility?view=sql-server-ver16&tabs=windows) The bulk copy program utility (bcp) bulk copies data between an instance of Microsoft SQL Server and a data file in a user-specified format.

## Programming 

- client programming to Microsoft SQL Server [ms lnk](https://learn.microsoft.com/en-us/sql/connect/homepage-sql-connection-programming?view=sql-server-ver16)

**Summary**
- ADO.NET is ideal when you need maximum control and performance, and you’re comfortable writing SQL queries and managing database connections manually.
- EF Core is better suited for rapid development and when you prefer working with higher-level abstractions and LINQ queries.

## MS DP-300 Database admin

- Github [link](https://github.com/MicrosoftLearning/dp-300-database-administrator/tree/master)

## Migration 

- https://learn.microsoft.com/en-us/data-migration/


## Back-up

- A BACPAC file is a ZIP file with an extension of BACPAC containing the metadata and data from the database. [MSLearn](https://learn.microsoft.com/en-us/azure/azure-sql/database/database-export?view=azuresql)

## Resoucres


Troubleshoot Azure SQL Performance
aka.ms/fixazuresqlperf

Azure SQL for Beginners
aka.ms/azuresql4beginners

Azure SQL Workshop
aka.ms/azuresqlworkshop

Azure SQL Fundamentals (with sandbox)
aka.ms/azuresqlfundamentals

Module 7 New
Performance Troubleshooting

Watch Data Exposed (Sql Edge?)
aka.ms/dataexposed

See more videos from our team
aka.ms/azuresqlyt

Follow us on social media
aka.ms/azuresqltwitter


sql-server-samples
https://github.com/microsoft/sql-server-samples/tree/master/samples/demos

DP-300 Db Adamin
https://learn.microsoft.com/en-us/credentials/certifications/azure-database-administrator-associate/?practice-assessment-type=certification#certification-prepare-for-the-exam