# vsc

- MS Learn modules for development with svc [ms learn](https://learn.microsoft.com/en-gb/training/paths/remote-development-vs-code/)

## dev containers


VSC Dev containers [docs](https://code.visualstudio.com/docs/devcontainers/containers)
Development Containers Specification  [docs](https://containers.dev/implementors/spec/)   
VSC Tutorial [link](https://code.visualstudio.com/docs/devcontainers/tutorial)
MS learn [module](https://learn.microsoft.com/en-gb/training/modules/use-docker-container-dev-env-vs-code/)

Prereq

- docker
- git
- vsc dev containers extension


Need a **.devcontainer** folder, containing

- devcontainer.json  
- When using template, select workspace

```
Open the project in a container
Press F1 to open the Command Palette.
Type reopen in container.
Select Dev Containers: Reopen in Container from the list of available options.
```

example [github spec](https://github.com/devcontainers/images/tree/main/src/python)

```
{
	"name": "Python 3",
	"image": "mcr.microsoft.com/devcontainers/python:1-3.11-bookworm"
}
```

### Adding VSC extensions with the dev container

- you can set vscode.settings to copy machine-specific settings into the container. You might have these settings in your own Visual Studio Code setup. By adding them to the settings, you ensure that anyone who opens this project gets these specific VS Code settings
- Search for an extension, right click 'add to dev contnainer' (?while container is running?)
- eg adding Jinja
```
{
	"name": "Python 3",
	// Or use a Dockerfile or Docker Compose file. More info: https://containers.dev/guide/dockerfile
	"image": "mcr.microsoft.com/devcontainers/python:1-3.11-bookworm",
	"customizations": {
		"vscode": {
			"extensions": [
				"wholroyd.jinja"
			]
		}
	}
}
```

### running commands after container creation/run

-  add the following to the  json:  "postCreateCommand": "pip3 install --user -r requirements.txt"

### restarting/rebuild

- Press F1 to open the Command Palette.
- Type rebuild and select Dev Containers: Rebuild Container.  ! hx not persisted
- [rebuild docs](https://code.visualstudio.com/docs/devcontainers/create-dev-container#_rebuild)

### Features


- [list of feature](https://containers.dev/features)
- eg
```
"features": {
    "ghcr.io/devcontainers/features/azure-cli:1": {
        "version": "latest"
    }
}
```
