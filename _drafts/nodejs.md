# Node JS 

Setting up Node, application and enviroments.


## Package Managers

npm, npx, and yarn are all package managers used in JavaScript development. Here's a brief explanation of each:

1. npm (Node Package Manager): npm is the default package manager for Node.js. It is used to install, manage, and publish packages/modules in the Node.js ecosystem. npm is installed along with Node.js, so you can use it right away.

2. npx (Node Package Runner): npx is a tool that comes bundled with npm since version 5.2.0. It allows you to run packages without installing them globally. It executes the package directly from the registry or from a local file. This is useful when you want to run a one-time command or use a package without installing it globally.

3. yarn: Yarn is another popular package manager for JavaScript, developed by Facebook. It was created to address some of the limitations of npm. Yarn offers faster and more reliable package installations by using a global cache and parallel downloads. It also provides a lockfile (yarn.lock) to ensure consistent installations across different environments.

In summary, npm is the default package manager for Node.js, npx is a tool that comes with npm to run packages without installing them globally, and yarn is an alternative package manager that offers faster and more reliable installations.

## Frontend Tooling

- Vite [site](https://vitejs.dev/)  Vite is a build tool and development server for modern web applications. It is specifically designed to optimize the development experience for frontend projects, particularly those using frameworks like Vue.js and React.

## SPA

| Framework      | Description                                                                                           | URL                                      |
|----------------|-------------------------------------------------------------------------------------------------------|------------------------------------------|
| React          | A JavaScript library for building user interfaces, known for its component-based architecture.       | [React](https://reactjs.org/)            |
| Angular        | A TypeScript-based open-source web application framework developed by Google.                      | [Angular](https://angular.io/)          |
| Vue.js         | A progressive JavaScript framework for building interactive web interfaces.                        | [Vue.js](https://vuejs.org/)             |
| Svelte         | A relatively new framework that compiles components into highly efficient vanilla JavaScript code.  | [Svelte](https://svelte.dev/)            |
| Ember.js       | A framework for building ambitious web applications, known for its convention over configuration.   | [Ember.js](https://emberjs.com/)        |
| Next.js        | A React framework that enables server-side rendering and static site generation for React applications. | [Next.js](https://nextjs.org/)          |

##  Bundlers

A bundler is a tool that takes multiple files and dependencies and bundles them together into a single file, typically for use in a web application. This helps optimize the loading and execution of the application by reducing the number of HTTP requests needed to fetch resources.

| Bundler   | Description                                                                                           | URL                                      |
|-----------|-------------------------------------------------------------------------------------------------------|------------------------------------------|
| Webpack   | A popular module bundler for JavaScript applications, capable of bundling assets and dependencies.   | [Webpack](https://webpack.js.org/)       |
| Parcel    | A zero-configuration bundler that automatically handles assets, dependencies, and code splitting.    | [Parcel](https://parceljs.org/)          |
| Rollup    | A module bundler that focuses on creating smaller, more efficient bundles for JavaScript libraries. | [Rollup](https://rollupjs.org/)         |
| Browserify| A simple tool for bundling Node.js modules for the browser, allowing the use of npm modules in the browser. | [Browserify](http://browserify.org/) |
| Vite      | A build tool that focuses on fast development server and optimized production builds for modern web projects. | [Vite](https://vitejs.dev/) |


## new appliction


The command 'npm init -y' is used to initialize a new Node.js project with a default package.json file. The '-y' flag stands for 'yes' and automatically accepts all the default options when creating the package.json file. This means that you don't have to manually answer the prompts that npm init usually presents.

```
mkdir my-express-app
cd my-express-app
npm init -y
npm install express```

[jump right in](https://learn.microsoft.com/en-us/training/modules/build-web-api-nodejs-express/3-exercise-create-app?tabs=github-codespaces)

to listen on 0.0.0.0
```
app.listen(port, '0.0.0.0', () => console.log(`Example app listening on http://0.0.0.0:${port}/`));
```


## Resources

There are several web frameworks available for Node.js, including Hapi, Fastify, Koa, and Express. Express is widely used due to its longevity, well-designed APIs, and prompt security patches.

[Build JavaScript applications with Node.js](https://learn.microsoft.com/en-us/training/paths/build-javascript-applications-nodejs/)