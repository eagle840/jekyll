# http security headers

Consider changing name of file, since we should include other items

- SOP: Same-Origin Policy (restricts)
  - where browsers? require same : [protocol]://[domain]:[port] to share resources
- CORS: Cross-Origin Resource Sharing  (enables)
  - CORS errors are in the browser console. (but not available to javascript)
  - server sends the responce header, allowing the browser to get data from foo.example (?must include protocol, domain, port*)
    - Access-Control-Allow-Origin: https:foo.example

- OWASP Secure Headers Project - https://owasp.org/www-project-secure-headers/#tab=Main
- nmap tool - https://github.com/nmap/nmap/blob/master/scripts/http-security-headers.nse


- https://www.keycdn.com/blog/http-security-headers

## Test your site

Use the browser developer tools to check the network section, and the requests, you should see a **response headers**

Note: that these services, and curl requests maybe blocked by WAF'S
- https://securityheaders.com/
- https://caniuse.com/?search=content%20security%20policy  eg, csp
- https://observatory.mozilla.org/
 - HTTP Observatory
 - TLS Observatory
 - SSH Observatory
 - [REDbot is lint for HTTP resources](https://github.com/mnot/redbot)
 - 3rd party

## Test your site for TLS and Compatibility

- https://www.ssllabs.com/ssltest/analyze.html (shows suites and versions)
- https://www.cyberssl.com/tools/sslchecker (no cipher suites)
  - TLS version
  - weak cipher check

## Learn about

- https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
- see related items from the above link

## Programming tools

### Python

- https://github.com/santoru/shcheck

## CSP Content Security Policy 

### Docs

- https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP#mitigating_cross-site_scripting
- https://www.keycdn.com/blog/http-security-headers

### Tools

- https://csp-evaluator.withgoogle.com/
- https://report-uri.com/home/generate    not sure how to use this


## can I use

- CSP - https://caniuse.com/?search=content%20security%20policy


## links

- Content Security Policy Reference [site](https://content-security-policy.com/)
- CSP policy generator [site](https://report-uri.com/home/generate)
- https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
- keycdn CSP guide [site](https://www.keycdn.com/support/content-security-policy)
- asp .net https://dev.to/fabriziobagala/http-security-headers-in-net-9go
- hapi js - https://hapi.dev/api/?v=21.3.0#-routeoptionssecurity
- header list - https://en.wikipedia.org/wiki/List_of_HTTP_header_fields#
- asp.net csp - https://learn.microsoft.com/en-us/aspnet/core/blazor/security/content-security-policy?view=aspnetcore-8.0
- .net - https://www.stackhawk.com/blog/net-content-security-policy-guide-what-it-is-and-how-to-enable-it/
- asp.net guide - https://arminzia.com/blog/the-aspnet-core-security-headers-guide/
- OWASP  HTTP headers  - https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html
- OWASP REST cheatsheet - https://cheatsheetseries.owasp.org/cheatsheets/REST_Security_Cheat_Sheet.html
- OWASP HTTP header cheatsheet - https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html
- OWASP CSP cheatsheet - https://cheatsheetseries.owasp.org/cheatsheets/Content_Security_Policy_Cheat_Sheet.html 

## asp.net

- enforce HTTPS in ASP>NET Core - https://learn.microsoft.com/en-us/aspnet/core/security/enforcing-ssl?view=aspnetcore-8.0&tabs=visual-studio%2Clinux-ubuntu
