# Using dotnet blazor (v8)

blazer  with dotnet 8

render modes
- SSR (static server-side render) -default
- Interactive server (orginal blazer model), using  signalR (called a circuit)
- Interactive WebAssembly - downloaded to browser
- Interactive Auto  - combo of above. Initialially runs on server, and then on client after code downloaded

Big change since Dotnet 7

Blazor Hybrid
- because the code can be downloaded, it can be run as a desktop or mobile(Muai) app.


Blazor components (inside a code file)
- like html, have the format <ComponentName></ComponentName>  Note they must start with a capital, including the file names


All components are nested.
- The root (in the components folder) being App.razor
- Routes.razor routes the components
- _Imports.razor, brings in common Razor directives

Projet root
- wwwroot  contains static files
- program.cs   entry point