# improve email security with DNS

# options

- SPF
- DKIM
- DMARC

# others

- TLS
- DNSSEC
- DANE
- SMTP MTA-STS

# Resources

https://www.hornetsecurity.com/en/services/email-authentification/