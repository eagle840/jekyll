# Application Insights


## Resources

- [Githut MS SI repos](https://github.com/orgs/microsoft/repositories?q=applicationinsights&type=all&language=&sort=)



## Cloud_RoleName

Applications seem to be seperated by 'Cloud_RoleName'[ms docs](https://learn.microsoft.com/en-us/azure/azure-monitor/app/app-map?tabs=net#set-or-override-cloud-role-name) also review correlation [ms docs](https://learn.microsoft.com/en-us/azure/azure-monitor/app/distributed-trace-data)
- both in requests and traces

```
union traces, requests
| distinct cloud_RoleName
```