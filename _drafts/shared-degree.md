---
---

# todo list for 'my-degree'

Need to put this somewhere open where people can copy it 
- google excel?
- todo?

### cloud

- Princples of Cloud Architectural Framework
    - THE FIVE PRINCIPLES
     1. Operational Excellence
     2. Security
     3. Reliabity
     4. Performance Efficiency
     5. Cost Optimization
    - aws: d1.awsstatic.com/whitepapers/architecture/AWS_Well-Architected_Framework.pdf
    - AZURE: https://docs.microsoft.com/en-us/azure/architecture/framework/
    - gcp: https://cloud.google.com/architecture/framework
    - nist: https://bigdatawg.nist.gov/_uploadfiles/M0008_v1_7256814129.pdf
- Backend,  FrontEnd, Full Stack, API's & Webhooks
- Cloud OS systems
- GCP, AWS, Azure
- CI/CD/IaC
- Multisystem virtualization
- Containerization Systems
- System migration
- Cloud based networking
- Performance, Logging, SRE
- Backup and DR
- DIY cloud with OpenStack
- ### Logs and Metrics
    * logs vs Metrics
    * #### Logs
        - types: local(console, terminal, buffered), Syslog, SNMP
        - Syslog messages: facility Codes, and Levels
        - Log formats:   CLF (Common Log Format), CEF (Common Event Format)
        - Tools: Logparser
    * Grafana
    * Kibana
    * Kibana
    * Prometheus
    * Logstash, fluetd
    * Elasticsearch


