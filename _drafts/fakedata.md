# fake data



## table/flat

- https://www.mockaroo.com/
- https://cobbl.io/
- https://fakerjs.dev




## stream

- https://awslabs.github.io/amazon-kinesis-data-generator/web/help.html
- https://github.com/MaterializeInc/datagen/


## other

- MS lignator  https://github.com/microsoft/lignator?tab=readme-ov-file

### MS Event Hub Data generator

- https://www.microsoft.com/en-gb/industry/blog/technetuk/2020/02/20/introducing-event-hubs-data-generator/#:~:text=Event%20Hub%20Data%20Generator%20(EHDG,it%20to%20Azure%20Event%20Hubs.
- https://github.com/JalalUddin7/EventHubDataGenerator