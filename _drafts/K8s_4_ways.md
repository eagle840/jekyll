# Kubernetes Four ways


An other view of the various ways to deploy a k8s cluster

## minikube

https://minikube.sigs.k8s.io/docs/start/


## kind 

https://kind.sigs.k8s.io/

windows docker desktop?


## k3s

https://github.com/k3s-io/k3s
https://k3s.io/


### others



# resources:

- https://shipit.dev/posts/minikube-vs-kind-vs-k3s.html
- https://www.cncf.io/wp-content/uploads/2020/08/CNCF-Webinar-Navigating-the-Sea-of-Local-Clusters-.pdf 
  https://www.youtube.com/watch?v=q6kyHDleioA  # for above
