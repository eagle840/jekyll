# Logging

## General

'12-factor-app' logs should be sent, unbuffered, to STDOUT

The environment, the stream should be captured, corilated and routed to a destination.
- EG Routers: lplex, Flentd, Fleunt Bit, Log stash)
- EG Desination: splunk


Grok can be used to take the text and generate a json from it

## Level

see syslog https://en.wikipedia.org/wiki/Syslog

| Level       | Description                                      |
|-------------|--------------------------------------------------|
| Emergency   | System is unusable                               |
| Alert       | Action must be taken immediately                 |
| Critical    | Critical conditions                              |
| Error       | Error conditions                                 |
| Warning     | Warning conditions                               |
| Notice      | Normal but significant condition                 |
| Informational | Informational messages                          |
| Debug       | Debugging messages                               |


## Grok

Grok is a tool used for parsing and matching log data. It helps in extracting structured data from unstructured log messages by using predefined patterns. Grok is commonly used in log management and analysis tools to make sense of log data and facilitate easier searching and filtering.


Online Grok Debugger: https://grokdebugger.com/

## Languages


| Language   | Error                  | Warning                | Information            | Debug                  | Verbose                | Trace/           | Critical               |
|------------|------------------------|------------------------|------------------------|------------------------|------------------------|------------------------|------------------------|
| ASP.NET    | `Trace.TraceError()`   | `Trace.TraceWarning()` | `Trace.TraceInformation()` | -                      | -                      | -                      | -                      |
| ASP.NET Core | `logger.LogError()`  | `logger.LogWarning()`  | `logger.LogInformation()`  | `logger.LogDebug()`      | -                      | `logger.LogTrace()`      | `logger.LogCritical()`  |
| Node.js (winston) | `logger.error()` | `logger.warn()`       | `logger.info()`         | `logger.debug()`        | `logger.verbose()`      | -                      | -                      |
| Node.js (console) | `console.error()` | `console.warn()`     | `console.info()`        | `console.debug()`       | -                      | -                      | -                      |
| Python     | `logging.error()`      | `logging.warning()`    | `logging.info()`        | `logging.debug()`       | -                      | -                      | `logging.critical()`    |


## Azure

https://learn.microsoft.com/en-us/training/modules/capture-application-logs-app-service/2-enable-and-configure-app-service-application-logging
