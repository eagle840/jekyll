---
---

# The Azure Montior Maze

While studing and trying to learn Azure Monitor for the AZ-500



# Running the Azure Monitor Maze
a review with Richard Benwell, (SquaredUp)
Mastering Azure Monitor (April 2020):   https://www.youtube.com/watch?v=_MtrZOMQ18M
Starts at 9:30



Picture at 26:30
oms: operations management suite
apm:
OP Insights ~ Splunk
full dagram on monitor hx @ 31:25

Kusto  db & sql language:  ~ spluck, Elastic etc.
   Standalone service is 'Azure Data explorer'
   KQL, blend of SQL & Powershell
   @ 34:40
https://dataexplorer.azure.com/clusters/help/databases/Samples

Platform Metric and logs
------------------------

Single azure resource monitor (eg VM/App Service), free, short term, most metrics, some logs (not os)
    In VM's you'll see enable: this bascilty installs MMA and sets up Log Analyics


!! look up the term 'Metric Namespace" wrt Azure, I think it should be similar to AWS's namespaces
!! Little finish flag -> 'supports filtering and grouping'
!! there appears to be a seperate (azure generated?) rg: 'dashboards'

A log is a collection of 'Events'
Some popular log formats:
- Windows Event Format
- Syslog
- Common Event Format
- JSON


App(lication) Insights
------------
An instance/resource by itself? (called a workspace???), try to have one for each application
For monitoring APM;  App type services, frameworks and code
Most of the items in this resource are generated from the signals (shown under Monitoring in the App Insight resource)
Full list @ 51:00

From the start App Insights is not figured for any apps.
Start in Investigate>Availability and add a TEST

(?? KQL can query over many dofferent App Insights??)
(you can upload logs into app insights)
(how do you connect an azure resource (eg App Service) to a App Insight)
   Goto the resource you want to monitor with App Insights, > Settings > Application Insights; select Turn On

The USAGE section is kinda like Google Analyics, good for getting business data

Log Analytics [MS Docs](https://learn.microsoft.com/en-us/azure/azure-monitor/logs/log-analytics-workspace-overview)
-------------

Two types of storage of logs (ie table type, shown as 'Plan')
- Analytics Logs; allows kql
- Basic; simple query only, only held for 8 days

List of Azure Monitor Log Analytics log tables
- https://learn.microsoft.com/en-us/azure/azure-monitor/reference/tables/tables-category


@1:14:00
Needs a log analyics workspace
Collect data from various resources
!WARNING collects logs for all VM logs then log selected

Can connect to onPrem SCOM
The DB *is*  >general settings>Logs
In the DB
 out of the box you get the LogManagement table (?contains the VM's - EG Events[across the rg?]?)

Consider using for a project (lifecycle), but remember those logs will pull in for all resources (when log selected)
Consider seting up a Log Analytics just for Log Activities so you can monitor what is going go across the Subscription/Tenant etc
the item 'Solutions' (below logs) is where you can setup pulling in additional ?3rd party? data sources)
Main data sources are in the Workspace Data Sources (VMs, Storage, Actvity logs) (also see Adv Settings below)
   SHOWS all resources,add which ones are connected to this (or other) Log Analytic Workspace
   You can have resouces sending data to multiple workspaces, but its difficult to find/setup in Azure
Selecting what logs you want to collect:
   is under: Settings> Adv Settings
   Main sections under here are:
      Connected sources; inc Win/Linux servers, Azure Storage, System Center (SCOM???)
      Data; here you define what data you want to pull in  ! remove this pulls in from every resource
      Computer Groups;

Workbooks: amix between a dashboard and a report pulled from Logs(db), can be pinned to a dashboard

Monitor
-------
Global  (per Tenant) UI
Access ALL metrics, logs, alerts and workbooks
Across ALL platform; App insights and Log Analytic instances

Alerts: again all alerts under the tenant
Metrics: Allows you to scope a resouces, inc app Insights, and log analytic workspaces
Logs: again, scopable

Insights; kinda like App insights, MS in building this

Signals (type)
-------

Azure Monitor supports several signal types, each serving different monitoring needs:

1. **Metric**: Quantitative data points collected at regular intervals, such as CPU usage, memory consumption, or network traffic.
2. **Log**: Records of events or transactions, including error logs, audit logs, and application logs.
3. **Activity Log**: Records of operations performed on Azure resources, like resource creation, deletion, or modification.
4. **Diagnostic Log**: Detailed logs from Azure resources that provide insights into their operation and performance.
5. **Azure Resource Health**: Signals related to the health of Azure resources, indicating their availability and performance status.
6. **Service Health**: Signals that provide information about the health of Azure services, including outages and planned maintenance.
7. **Application Insights**: Telemetry data from applications, including request rates, response times, and failure rates.

These signal types help you monitor and manage the performance, health, and availability of your Azure resources effectively. If you need more details on any specific signal type, feel free to ask!

Alerts
------
Unified alerting

Other Resources:
12 part series:
   https://cloudadministrator.net/2019/08/15/azure-monitor-alerts-series-part-1/


- Rules, action groups, Alert Rules


## Mermaid

flowchart TD
 subgraph s1["Target_scopes"]
        A1["scope target"]
        A2["Type"]
        A3["signal type"]
        A4["status"]
  end
 subgraph s2["Signals"]
        B1["Types"]
        B2["metrics"]
        B3["log"]
        B4["Activity"]
        B5["Smart Detector"]
  end
 subgraph s3["Conditions"]
        n4["Untitled <br />Node"]
  end
 subgraph s4["Subpression/None"]
  end
 subgraph s5["Alerts"]
        E1["servirty"]
        E2["Service"]
        E3["AppIn SIghts"]
        E4["alert contion: <br />Fired/Resolved"]
        E5["resource"]
        E6["user_response"]
        E7["Action Group triggered"]
  end
 subgraph s6["Action_groups"]
        F1["SMS/email"]
        F2["Automatioin"]
  end
    n1 --> n2
    s2 --> s1
    s3 --> s2
    s4 --> s3
    s5 --> s4
    s6 --> s5

## Alert  (also see azure-alerts.md)

- severity
- alert condition
- user response (new -> Ack'ed =-> closed)


## Action Group



## Alert Rule

### Metric  (other types?)

- Scope
- Actions (Action group)
- Conditions 
- Severity level to trigger
- description for alert


### Creating 

1) scope
2) Condition (select a signal)
3) Actions (Action group)
4)


### Severity 

0 - critical
1 - error
2 - warning
3 - informational
4 - verbose


DashBoards (generic term)
----------
Azure Monitor; built in
Grafana; graphing
squaredUp;  sharing with business stakeholders

Workbooks
---------

Workbooks provide a flexible canvas for data analysis and the creation of rich visual reports within the Azure portal.   
[workbooks doc](https://learn.microsoft.com/en-us/azure/azure-monitor/visualize/workbooks-overview)

Workbooks can extract data from these data sources:

- Logs
- Metrics
- Azure Resource Graph
- Azure Resource Manager
- Azure Data Explorer
- JSON
- Merge
- Custom endpoint
- Workload health
- Azure resource health
- Azure RBAC
- Change Analysis
- Prometheus

You can use Azure Resource Graph Explorer for grab lists

```Azure Resource Graph Explorer
Resources
| where resourceGroup startswith "b105d"
```



# Service Health Alerts


'heath and suport' > 'Service health'  & 'Health Alerts' if created

look for 'action required'

# service Helath alert (subscription based)

same location 'Creat Service health alert'

select subscription ( for each)

Actions is the already Azure Monitor action groups, these alerts are marked as Signal type: 'Service Health'

Alert Rules are trigger from 'service health' logs, that show up in Activity Log


see https://www.youtube.com/watch?v=KwQBBvyOblk

# Discover resouces

List of azure resource provider

https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/azure-services-resource-providers

Azure Resource Graph Explorer 

microsoft.insights/components   microsoft.insights=> resouce provide     components=> resource type

Doc list of providers https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/azure-services-resource-providers

portal for providers and types: 'Resource Explorer'

- the portal allows search by
   - Providers; a full list of providers and type of all azure items
   - Subscriptions; a list of items that have been provisioned through subscriptions - but note the responce is slow

 KQL

resources
| where type == 'microsoft.insights/components'
| where isnull( properties['WorkspaceResourceId'])

## "resources graph explorer"

explore by Categories OR table, and select scope by Directory, Mgmnt Grp, or Subscription

- allows you to run KQL against resources
- over view: https://www.youtube.com/watch?v=jXng4Y5cHf8
- 'Azure Resource Explorer and Azure Resource Graph: Unsung Heroes of the Cloud ep. 1' https://www.youtube.com/watch?v=Ux9vIl3XnpQ
- Azure Resource Graph Deep Dive (J.Savill) https://www.youtube.com/watch?v=gkOh4MjhxIs

## Common Alert Schema

https://learn.microsoft.com/en-us/azure/azure-monitor/alerts/alerts-common-schema

## Example Queries

### Monitor multiple Availibility Tests (through multiple App Insight - LAWS)

- Use Monitor > Logs
- Set scope to the resouces you need. (each Appplication Insights for each resource you need)

```kql
availabilityResults 
| summarize Passed = countif(message == "Passed"), Failed = countif(message != "Passed") by name
```

Check the availibilty test results. Note that the scope need to be set for the AppInsights that you want to monitor.
```kql
availabilityResults 
| summarize Passed = countif(message == "Passed"), Failed = countif(message != "Passed") by name
```