# List of network tools


# Tracert

- keycdn https://tools.keycdn.com/traceroute   # tracert through multiple locations to a single IP

# Powershell

- https://learn.microsoft.com/en-us/powershell/module/nettcpip/test-netconnection?view=windowsserver2022-ps

## Pathping

- pathping

## Telnet

- use telnet like nc to attempt to open a port

## nmap

- nmap via thm

# info

- https://mxtoolbox.com/

# looking glass

- https://tools.keycdn.com/bgp-looking-glass

# peering

- https://www.peeringdb.com/advanced_search

# stats

- https://stat.ripe.net/app/launchpad

there are free services available for monitoring the performance of Autonomous System (AS) networks. Some of these services offer basic monitoring capabilities and insights into network performance metrics, while others may have limitations on the number of ASNs or features available for free. Here are a few free services that you can consider for monitoring AS network performance:

1. **RIPEstat**: RIPEstat is a free service provided by the Réseaux IP Européens Network Coordination Centre (RIPE NCC) that offers various tools for analyzing and monitoring internet infrastructure, including ASNs, IP prefixes, and BGP routing information.

2. **BGPView**: BGPView is a free online tool that provides information about BGP routing, ASNs, IP prefixes, and network connectivity. It allows you to visualize BGP routing paths, monitor routing changes, and analyze network reachability.

3. **Hurricane Electric BGP Toolkit**: Hurricane Electric offers a free BGP Toolkit that provides tools for analyzing BGP routing tables, ASNs, IP prefixes, and network connectivity. It includes features such as BGP looking glass, route server, and BGP route propagation analysis.

4. **PeeringDB**: PeeringDB is a free online database of networks and interconnection data that allows you to search for ASNs, network operators, and peering information. It can be useful for researching peering relationships and network connectivity.

While these free services offer valuable insights into AS network performance, it's important to note that they may have limitations in terms of features, data accuracy, and real-time monitoring capabilities compared to paid monitoring services. Depending on your specific monitoring requirements, you may also consider commercial monitoring tools and services for more advanced monitoring and analysis of AS networks.