# Azure DP-203 Data Engineer


## Open source Vs Azure

- Batch workload: Data Factory/ Synapse Vs Apache Airflow
- Streaming: Stream Analytics VD Apache NiFi

## Group 1

Partitioning

```
loT structure:
{Region}/{SubjectMatter(s)}/(yyyy}/(mm}/{dd}/{hh}/

Batch jobs structure:
.{Region}/{SubjectMatter(s)}/In/{yyyy}/{mm}/{dd}/{hh
.{Region}/{SubjectMatter(s)}/Out/(yyyy}/{mm}/{dd}/{hh]/
.{Region}/{SubjectMatter(s)}/Bad/(yyyy}/(mm}/{dd}/{hn]/

Time-series structure:
/DataSet/YYYY/MM/DD/datafile_YYYY_MM_DD.tsv
```

partition types
- Round Robin
- Hash
- Dynamic Range
- Fixed Range
- Key

What is: "consider using data flows in Azure Front Door to partition data when writing files to ADLS Gen2 or Blob storage"


---
Benefits of table partitioning
. Improves efficiency and performance of loading and querying by limiting the scope to a subset of data.
· Offers significant query performance enhancements.
· Allows Multi-Column Distribution (MCD) that helps:
· distribute data on multiple columns to balance the data distribution in tables
· reduce data movement during query execution.

A successful partitioning scheme usually has tens to hundreds of partitions, not thousands.

SQL Server and dedicated SQL pool support one partition column per table that is ranged partition.

### Partition in Blob

Partitioning can improve scalability, reduce contention, and optimize performance.
· The partitioning strategy must be chosen carefully to maximize the benefits while minimizing adverse effects.
. Scale targets must be considered for blob storage and storage accounts.

#### Partitioning Azure Blob Storage

- The partition key for a blob: account name + container name + blob name.

- Blobs are distributed across many servers to scale out access to them. Note that a single blob can only be served by a single server.

- The naming convention that uses timestamps or numerical identifiers can lead to excessive traffic going to one partition.

- The actions of writing a single block or page are atomic. The operations that span blocks, pages, or blobs are not atomic.

## Azure Synapse


## Data operations
As a data engineer some of the main tasks that you'll perform in Azure include data integration, data transformation, and data consolidation.

### Data integration

Data Integration involves establishing links between operational and analytical services and data sources to enable secure, reliable access to data across multiple systems. For example, a business process might rely on data that is spread across multiple systems, and a data engineer is required to establish links so that the required data can be extracted from all of these systems.

### Data transformation


Operational data usually needs to be transformed into suitable structure and format for analysis, often as part of an extract, transform, and load (ETL) process; though increasingly a variation in which you extract, load, and transform (ELT) the data is used to quickly ingest the data into a data lake and then apply "big data" processing techniques to transform it. Regardless of the approach used, the data is prepared to support downstream analytical needs.

### Data consolidation


Data consolidation is the process of combining data that has been extracted from multiple data sources into a consistent structure - usually to support analytics and reporting. Commonly, data from operational systems is extracted, transformed, and loaded into analytical stores such as a data lake or data warehouse.

## Big Data Architecture

### Ingest
 - Batch
   - pipelines in Azure Synapse Analytics or Azure Data Factory
- Realtime
   - Apache Kafka for HDInsight or Stream Analytics

### Store
- Azure datalake

### Prep & Train
- Azure Synapse Analytics, Azure Databricks, Azure HDInsight, and Azure Machine Learning.

### Model & Serve
- Microsoft Power BI, or analytical data stores such as Azure Synapse Analytics

## 3 V's

- volume, variety & velocity

## Four main functions

- Data Movement
- Data Ingestion
- Data storge
- Data Transformation

## ETL vs ELT

ETL is transformed and stored, ELT is moved, and then transformed when needed.

### Cases for ETL

- Privacy or security concerns (- less movement and risk)
- Compliance
- Small datasets
- Legacy infratstructure


### Cases for ELT

- Data lakes!
- Large data
- Mutiple uses
- Speed
- cost

### ACID

- Atomicity: Everything or nothing at all
- Consistency: Consistent before and after the transaction
- Isolation: Everything occurs independently
- Durability: Can overcome system failure

## Schema (logical)

- Flat (eg Excel)
- Hierarchical (tree)
- Relational
  - Star
    - Easy to query, one fact table, one dimension-table level, not normalized (denormalized - redundant data)
  - Snowflake
    - Mulitple dimension tables, reduced query preformance, normalized

## DataBricks **Medallion** Architecture

- Bronze: raw
- Silver: Processed (clean and transformed)
- Gold: Presented (Business insights)

## Data warehouse, etc vs

- Data warehouses: relational, use BASE transactions
- Data lake
- SQL DB
- SWL-DW

## Data Processings

- The process of collecting data and changing it into USABLE information
- Six stages
  - 1. Data Collection (ingestion) (bronze: raw, unclean data)
  - 2. Data preparation (data cleaning) (silver: cleaned data)
  - 3. Data input (moving cleaned data)
  - 4. Processing (inc ml)
  - 5. Data output (insights) (gold: processed data for insight)
  - 6. Data Storage (for future use)

| Data Processing Step | Azure Services |
|----------------------|----------------|
| 1. Data Ingestion    | Azure Data Factory, Azure Event Hubs, Azure IoT Hub |
| 2. Data Storage      | Azure Blob Storage, Azure Data Lake Storage, Azure SQL Database |
| 3. Data Preparation  | Azure Databricks, Azure HDInsight, Azure Synapse Analytics |
| 4. Data Transformation | Azure Data Factory, Azure Functions, Azure Logic Apps |
| 5. Data Analysis     | Azure Synapse Analytics, Azure Machine Learning, Azure Data Explorer |
| 6. Data Visualization | Power BI, Azure Data Studio, Azure Dashboards |

## Partitioning


- Preformance (easier to query,load, access)
- Availability (replication)
- Scalability (horizontal scaling)

Types:

- Vertical: spit by column (id, col1, col2)(id,col3,col4)
- Horizonal: spit by rows  (id1, id2,id3)(id4,id5,id6)
- WIP need a better description on these 2


Designing:
- Now and future
- Design for Scalability
- Design for Query Performance
- Design for Availablity

Logical Vs Physical
- Logical are stored in phyical, multiple logical can be stored in one phyical

Hot Partition: where there is more data stored in one partition compared with the others, **or** throughout on a partition



## File Types (for big data)

### Avro

- Row based
- Schema stored in JSON
- Data stored in binary
- benefits:
  - Easily handles schema changes
  - Great for write-heavy workloads
  - Great for ETL ops that need all columns

### Parquet

- Column Based
- Nested data structures
- Data stored in binary
- benefits
  - great for wide-column quieres
  - efficient for read-heavy workloads
  - easily queries a subset of columns or nested data

### ORC (Optimized Row Container)

- Kinda mix of the two above
- column based, but with stripes of rows
- data stored in binary
- benefits
  - supports ACID properties
  - very efficient compression
  - good for large efficient reads

## Stream Analytics

### Components

- Input
- Query (transformation)
- Output

### Jobs

- `Embarrassingly Parallel Jobs`
  - have equal i/p and o/p partitions
  - most efficient jobs


## Blob

Types:
 - general (std)
 - Block (premium)
 - Page (premium)

 Account
   - Container
     - Blob

### Using Partitioning

Uses Range-based paritioning (lexical ordering and timestamps)
Partition Key = Aacount + Container + Blob

#### Best Practices

1. Avoid slowly changing timestamps (yyyymmdd)
2. Name based upon likely queries
3. Avoid latency-causing partitioning
    a. Use blob sizes greater than 256 KiB
    b. Consider using a hashing function if necessary