# Architecture decision records


An Architectural Decision (AD) is a justified design choice that addresses a functional or non-functional requirement that is architecturally significant

## Document links

- GCP [docs](https://cloud.google.com/architecture/architecture-decision-records)
- Azure [docs](https://learn.microsoft.com/en-us/azure/well-architected/architect-role/architecture-decision-record)
- AWS Best 2 minute read[docs](https://docs.aws.amazon.com/prescriptive-guidance/latest/architectural-decision-records/adr-process.html#:~:text=An%20architectural%20decision%20record%20(ADR,and%20therefore%20follow%20a%20lifecycle.)
- Github [Best Resourse](https://adr.github.io/)
- Markdown Architectural Decision Records - Part of Github ADR [doc](https://adr.github.io/madr/)
- Joel Parker Henderson [another good github ref](https://github.com/joelparkerhenderson/architecture-decision-record)