# Azure DevOps



generator: https://azuredevopsdemogenerator.azurewebsites.net/

## links

- https://devblogs.microsoft.com/devops/
- azure devOps pipeline (eg ant) https://learn.microsoft.com/en-us/azure/devops/pipelines/tasks/reference/ant-v1?view=azure-pipelines
- Azure sdks and tools.  https://azure.microsoft.com/en-us/downloads/
- Azure various languages sdks (this is to python)  https://azure.github.io/azure-sdk/releases/latest/python.html


## Pipelines

Edditing the pipeline in the pipelines section gives you access to intelisence and vadition. 

Although the interfaces doesn't suggest this, they are broken up into two types [yt link ](https://www.youtube.com/watch?v=3cGtA__dKUc)

- Classic; This GUI based, and dividend into 
  - **Pipelines > Pipeline** with GUI interface
  - **Pipelines > Releases** with GUI interface 
- YAML
  - these pipelines are controlled with a yaml file in the repo.
  - the pipeline that is run, is the pipeline in that branch.

- environments
  - So far you have delt with environments with no resources. But you can but gates on them (see environments blade)
  - ```
    jobs:
    - deployment: DeployTo${{ parameters.Environment }}
      displayName: "Deploy To ${{ parameters.Environment }}"
      environment: Doc Capture - ${{ parameters.Environment }}
    ```


## variables

- predefined eg Build.SourceBranch: https://learn.microsoft.com/en-us/azure/devops/pipelines/build/variables?view=azure-devops&tabs=yaml
- to printout: `echo SourceBranch:$(Build.SourceBranch)` # just like this, no spaces
  - ```
- stage: TroubleShoot
  jobs:
  - job: Test1
    steps:
    - script: |
        echo Hello Stage B!
        echo $(isMain)
        echo SourceBranch:$(Build.SourceBranch)
        echo SourceReason:$(Build.Reason)
        echo SourceBranchName:$(Build.SourceBranchName)
        echo SystemPullRequestIsFork:$(System.PullRequest.IsFork)
    ```
- other: https://learn.microsoft.com/en-us/azure/devops/pipelines/process/variables?view=azure-devops&tabs=yaml%2Cbatch

## Environments


[yt link](https://www.youtube.com/watch?v=gN4j65w7wIM)

For use with YAML pipelines, allowing control over the pipelines

They are defined in the yaml file under:
Stages>jobs>Deployments section. K:V enviroment: env_name

and can have an associated section: 'strategy'

## Artifact

- Artifacts explained [yt link](https://www.youtube.com/watch?v=WWCmEUCt3Cc)

| BUILD ARTIFACTS            |    PIPELINE ARTIFACTS        |     AZURE ARTIFACTS       |
|----------------------------|------------------------------|---------------------------|
| Older                      | Newer                        | Different service         |
| Classic & YAML             | YAML                         | Classic & Yaml            |
| Slow                       | fast                         | fast                      |
| Tied to a pipeline run     | tied to a pipeline run       | independent               |
| Can trigger CD             | Can trigger CD               | can trigger CD            |
| Cannot be shared           | cannot be shared             | Shareable, x org          |
| Can store any where        | can store anywhere           | Typed packages            |
| Free                       | Free                         | 2GB free, then $          |

There is a difference between 'PublishBuildArtifacts' and 'PublishPipelineArtifacts'

## Other

- https://techcommunity.microsoft.com/t5/healthcare-and-life-sciences/azure-devops-pipelines-environments-and-variables/ba-p/3707414