# Azure workbooks.

Workbooks can be found in various locations:
- Monitor
- App Insights
- In a resources group id-(name)

## Links

Azure Workbooks (In azure Monitor) [link](https://learn.microsoft.com/en-us/azure/azure-monitor/visualize/workbooks-overview)

Suggest you review KQL and Azure Graph Explorer


I'm not sure were work books are collected, it appears a RG (& location), but it look like they show up in other places, eg Azure Monitor


## Starting a workbook

1) consider where you're going to store it.

2) Create a Text box for a general description of workbook, and other requirements.

3) create a parameter box, and add subscriptions

# Workbook galleries

- Available as a Gallery Template or ARM Template
- [Azure Monitor Workbook Templates](https://github.com/microsoft/Application-Insights-Workbooks?ref=huuhka.net)
- [SIEM related workbooks](https://github.com/azure-ad-b2c/siem?ref=huuhka.net)

## Results

### Grouping

https://www.cloudsma.com/2021/01/group-by-azure-workbooks/#:~:text=Metric%20Widget&text=Open%20%E2%80%9CColumn%20Settings%E2%80%9D%20in%20your,then%20Rows%20Labelled%20by%20Name.

# Example scripts

### All DNS names that a Azure ASP uses

I looked at the json template for the created service to work out the JSON from (properties.hostNames)

```
Resources
| where type == 'microsoft.web/sites'
| extend hostNames = parse_json(properties.hostNames)
| project name, hostNames
```

# Resources

- [MS Docs on workbooks](https://learn.microsoft.com/en-us/azure/azure-monitor/visualize/workbooks-overview)
- [Tips for using Azure Workbooks - Part 1](https://www.huuhka.net/tips-for-using-azure-workbooks/)
- [Tips for using Azure Workbooks - Part 2](https://www.huuhka.net/tips-for-using-azure-workbooks-part-2/)
- [Tips for using Azure Workbooks on IaC - Part 3](https://www.huuhka.net/devops-for-azure-workbooks/)
- [Using log quieries](https://ronaldbosma.github.io/blog/2023/02/28/azure-workbook-tips-tricks/)
- [Workbook samples on github](https://github.com/Microsoft/Application-Insights-Workbooks/blob/master/Documentation/Samples/Samples.md)

```
let subscriptionFiller = dynamic([{Scription}]) # 'Subscription is the parameter subscription picker, the [] are a list, dynamic a f90, returns an array
```