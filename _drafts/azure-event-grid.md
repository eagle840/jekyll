# Azure event Grid


https://learn.microsoft.com/en-us/azure/event-grid/

## Schema's

- Azure event grid
- Cloud Event schema (v1.0)
- Custom

Other services that support Cloud Event schema:

1. Amazon EventBridge: Amazon EventBridge supports Cloud Events as a standard format for event data, allowing seamless integration with various AWS services and third-party applications.

2. Google Cloud Pub/Sub: Google Cloud Pub/Sub can publish and consume events in the Cloud Events format, enabling interoperability with other event-driven systems.

3. Microsoft Azure Event Grid: Azure Event Grid has added support for Cloud Events, allowing users to send and receive events in the Cloud Events format within the Azure ecosystem.

4. Kubernetes: Kubernetes has adopted Cloud Events as a standard for representing events within the Kubernetes ecosystem, making it easier to integrate Kubernetes events with other systems.

5. Apache Kafka: Apache Kafka can be configured to produce and consume events in the Cloud Events format, enabling compatibility with event streaming applications that support Cloud Events.


