# Azure Container Registry


for ACI, see azure-aci.md


## Capabilites

- Supports Linux, Windows, ARM
- cache
- tokens (authzn)
- Tasks
- endpoint : <resouce-name>.azureacr.io/<repoNameSpace>:<tag>




## Container

## Registry

#### Tiers

- Basic: 10Gb
- Std: 100Gb
- Prm: 500Gb, Gep Rep


## quick Cmds

`az acr build -t helloworld:{{.Build.ID}} -t helloworld:release -r eagle840 .`

-r <registry> # <registry name>.azurecr.io
helloworld: is the **repo**, can contain / eg sample/helloworld
release/{{.Build.ID}} is the **tag**
. is the location of the Dockerfile

[Link for acr build api](https://learn.microsoft.com/en-us/cli/azure/acr?view=azure-cli-latest#az-acr-build)

## cmd

PURGE_CMD="acr purge --help"

az acr run --cmd "$PURGE_CMD" --registry eagle840 /dev/null


PURGE_CMD="acr  --help" # returns:
```
Welcome to the Azure Container Registry CLI!

To start working with the CLI, run acr --help

Usage:
  acr [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  login       Login to a container registry
  logout      Log out from a container registry
  manifest    Manage manifests inside a repository
  purge       Delete images from a registry.
  tag         Manage tags inside a repository
  version     Print version information

Flags:
  -c, --config stringArray   Auth config paths
  -h, --help                 Print usage
  -p, --password string      Registry password
  -r, --registry string      Registry name
  -u, --username string      Registry username

Use "acr [command] --help" for more information about a command.



## Tasks

(see aalso build packs)

- Needs a 'source coda context' (GH, AzDevOps, local)
  - docker format
  - `az acr build`
  - az acr cmds are logged! be aware of senistive data
- AuthZ/N
  - Individual Id (az acr login)
  - Headless/Service Id  (service principle)
  - tokens (fine grain, time limited)


### Quick

### Auto

### Multistep

## Build Backs (In Azure preview)

Building images without a docker file

https://buildpacks.io/

`az acr pack build` [Docs](https://learn.microsoft.com/en-us/cli/azure/acr/pack?view=azure-cli-latest#az-acr-pack-build)