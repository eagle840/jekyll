# SQL


[SQL-92](https://en.wikipedia.org/wiki/SQL-92), also known as SQL2, is a widely adopted standard for the Structured Query Language, defining the syntax and semantics for querying and managing relational databases. It introduced important features such as outer joins, subqueries, and support for triggers and stored procedures, shaping the development of SQL as a powerful and versatile database language. Managed my ANSI and ISO

[Transact-SQL](https://en.wikipedia.org/wiki/Transact-SQL) (T-SQL) is [Microsoft](https://learn.microsoft.com/en-us/sql/t-sql/language-reference?view=sql-server-ver16)'s and [Sybase](https://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.infocenter.dc38151.1510/html/iqrefbb/Tsos.htm)'s proprietary extension to the SQL (Structured Query Language) used to interact with relational databases. T-SQL expands on the SQL standard to include procedural programming, local variables, various support functions for string processing, date processing, mathematics, etc. and changes to the DELETE and UPDATE statements


## Tools

- Mermaid [Entity Relationship Diagrams](https://mermaid.js.org/syntax/entityRelationshipDiagram.html)
- SSMS [download](https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16)     Azure [Docs](https://learn.microsoft.com/en-us/azure/azure-sql/database/connect-query-ssms?view=azuresql)
- Azure [Data Studio](https://learn.microsoft.com/en-us/azure-data-studio/download-azure-data-studio?tabs=win-install%2Cwin-user-install%2Credhat-install%2Cwindows-uninstall%2Credhat-uninstall)
- [bcp](https://learn.microsoft.com/en-us/sql/tools/bcp-utility?view=sql-server-ver16&tabs=windows) The bulk copy program utility (bcp) bulk copies data between an instance of Microsoft SQL Server and a data file in a user-specified format.

## DDL and DML

DDL (Data Definition Language) and DML (Data Manipulation Language) are two types of SQL (Structured Query Language) commands used to interact with databases. Here's an explanation of each:

### DDL (Data Definition Language)
DDL stands for Data Definition Language. It is a subset of SQL (Structured Query Language) used to define and manage the structure of database objects. DDL commands are used to create, modify, and delete database objects such as tables, indexes, views, and schemas. Some common DDL commands include:

- `CREATE`: Used to create new database objects like tables, indexes, or views.
- `ALTER`: Used to modify the structure of existing database objects.
- `DROP`: Used to delete database objects.
- `TRUNCATE`: Used to remove all records from a table.

### DML (Data Manipulation Language)
DML stands for Data Manipulation Language. It is a subset of SQL used to manipulate data stored in the database. DML commands are used to retrieve, insert, update, and delete data in database tables. Some common DML commands include:

- `SELECT`: Used to retrieve data from one or more tables.
- `INSERT`: Used to add new records to a table.
- `UPDATE`: Used to modify existing records in a table.
- `DELETE`: Used to remove records from a table.

In summary, DDL is used to define and manage the structure of database objects, while DML is used to manipulate the data stored in those objects. Both DDL and DML are essential components of SQL that are used to interact with and manage databases.


# Transactions

In SQL, a transaction represents a sequence of one or more SQL statements executed as a single unit of work. Here are the key points about transactions:

1. Atomicity: A transaction is either completely successful or completely unsuccessful. If any part of it fails, the entire transaction is rolled back.
2. Consistency: Transactions maintain integrity by moving the database from one valid state to another.
3. Isolation: Concurrent transactions are isolated from each other to ensure data accuracy.
4. Durability: Once committed, transaction modifications remain even after system failures.

To control transactions, you can use the following commands: (need to confirm)

`BEGIN TRANSACTION`: Marks the start of an explicit or local transaction.
`SET TRANSACTION`: Sets properties like isolation level and access mode.
`COMMIT`: Saves all changes since the last commit or rollback.
`ROLLBACK`: Reverses changes made since the last commit or rollback

## Schemas

In SQL, the dot (.) in **SalesLT.Customer** separates the schema name from the table name. Here’s a breakdown:

SalesLT: This is the schema name. Schemas are used to organize and group database objects, such as tables, views, and stored procedures. They help manage permissions and can make it easier to understand the structure of the database.
Customer: This is the table name within the SalesLT schema.
Using the schema name before the table name ensures that SQL queries are clear about which table is being referenced, especially in databases with multiple schemas12.

You can also use a SQL query to list all objects within the SalesLT schema:
```sql
SELECT 
    schema_name(schema_id) AS SchemaName,
    name AS ObjectName,
    type_desc AS ObjectType
FROM 
    sys.objects
WHERE 
    schema_name(schema_id) = 'SalesLT';

```

### For dbo.Table

Yes, dbo stands for Database Owner. It’s a special schema in SQL Server that is the default schema for new objects created by the database owner. Here are a few key points about dbo:

1. Default Schema: When you create a new table without specifying a schema, it defaults to the dbo schema. For example, if you create a table named Course without specifying a schema, it will be created as dbo.Course.
2. Permissions: The dbo schema has full permissions on the database. This means that any object created under the dbo schema can be accessed and modified by users with the appropriate permissions.
3. Organization: Using schemas like dbo helps in organizing database objects. It allows you to group related objects together and manage permissions more effectively.
4. Compatibility: Many applications and scripts assume the use of the dbo schema, so using it can help ensure compatibility and reduce potential issues.
You can find the dbo schema and its objects in SQL Server Management Studio (SSMS) by expanding the database and then the “Schemas” folder, similar to how you would find the SalesLT schema.

### To create a new schema

see: [MS learn](https://learn.microsoft.com/en-us/sql/relational-databases/security/authentication-access/create-a-database-schema?view=sql-server-ver16)

```sql
CREATE SCHEMA NewSchemaName AUTHORIZATION dbo;
```


----
Q: I'm creating a cheat sheet on SQL. So far I have DDL and DML, what other features should be in the cheatsheet

A:

That's a great start! Here are some other important SQL features that you may want to include in your cheat sheet:

1. DCL (Data Control Language): Includes commands like GRANT and REVOKE for managing user permissions and access control.

2. TCL (Transaction Control Language): Includes commands like COMMIT, ROLLBACK, and SAVEPOINT for managing transactions.

3. Data types: Common data types in SQL such as INT, VARCHAR, DATE, and BOOLEAN.

4. Joins: Different types of joins like INNER JOIN, LEFT JOIN, RIGHT JOIN, and FULL JOIN for combining data from multiple tables.

5. Aggregate functions: Functions like SUM, AVG, COUNT, MIN, and MAX for performing calculations on groups of rows.

6. Subqueries: Using subqueries to nest one query within another query.

7. Indexes: Creating and using indexes to improve query performance.

8. Constraints: Adding constraints like PRIMARY KEY, FOREIGN KEY, UNIQUE, and NOT NULL to enforce data integrity.

9. Views: Creating virtual tables using views to simplify complex queries.

10. Functions: Creating user-defined functions to encapsulate logic and reuse code.

Including these features in your SQL cheat sheet will provide a comprehensive reference for working with SQL databases.

| Feature           | MySQL              | PostgreSQL         | Microsoft SQL Server | Oracle Database    | SQLite            |
|-------------------|--------------------|--------------------|----------------------|--------------------|-------------------|
| License           | Open Source        | Open Source        | Proprietary          | Proprietary        | Public Domain     |
| ACID Compliance   | Yes                | Yes                | Yes                  | Yes                | Yes               |
| Replication       | Yes                | Yes                | Yes                  | Yes                | No                |
| Partitioning      | Yes                | Yes                | Yes                  | Yes                | No                |
| Full-text Search  | Yes                | Yes                | Yes                  | Yes                | No                |
| JSON Support      | Yes                | Yes                | Yes                  | Yes                | Yes               |
| Window Functions  | Limited            | Yes                | Yes                  | Yes                | No                |
| Stored Procedures | Yes                | Yes                | Yes                  | Yes                | Yes               |
| Triggers          | Yes                | Yes                | Yes                  | Yes                | Yes               |
| Cost              | Free               | Free               | Paid                 | Paid               | Free              |

## Note on Azure SQL db

The master database on a server has to be accessed through an application

- users are in the 'security' under >Databases>System Databases>master>Security>Users


You need to add users to the Master before adding them to the usable database       

EG

```
--run in master (note that logins are created before user)
CREATE LOGIN [data_analyst] WITH PASSWORD = 'wb#rR1Aks4ZGv2S#'
GO
CREATE USER [data_analyst] FOR LOGIN [data_analyst];
---
-- run at user database level
CREATE USER [data_analyst] FOR LOGIN [data_analyst] WITH DEFAULT_SCHEMA=[globomanticsDB];
ALTER ROLE db_datareader ADD MEMBER [data_analyst];
```    

### In Master database


- Master logins (note that logins are created before user)
```
SELECT name
FROM sys.sql_logins
WHERE type_desc = 'SQL_LOGIN'
```

- Master roles
```
SELECT name
FROM sys.database_principals
WHERE type_desc = 'DATABASE_ROLE'
```

types  (R)DATABASE_ROLE, (S)SQL_USER, (E)EXTERNAL_USER, (X)EXTERNAL_GROUP !see the difference in the above 2 queiries 


- Master users and associated login (?? check - they maybe the same thing)
```
SELECT DP.name AS UserName, SL.name AS LoginName
FROM sys.database_principals DP
JOIN sys.sql_logins SL ON DP.sid = SL.sid
WHERE DP.type_desc = 'SQL_USER'
```

- Master Roles and Associated Users
```
SELECT dp.name AS UserName, dp.type_desc AS UserType, 
       dp2.name AS RoleName, dp2.type_desc AS RoleType
FROM sys.database_role_members drm
JOIN sys.database_principals dp ON drm.member_principal_id = dp.principal_id
JOIN sys.database_principals dp2 ON drm.role_principal_id = dp2.principal_id
WHERE dp.type_desc = 'SQL_USER'
```


### In INUSE database

- list users
```
SELECT name
FROM sys.database_principals
WHERE type_desc = 'SQL_USER';
```

- list roles
```
SELECT name
FROM sys.database_principals
WHERE type_desc = 'DATABASE_ROLE';
``` 

types  (R)DATABASE_ROLE, (S)SQL_USER, (E)EXTERNAL_USER, (X)EXTERNAL_GROUP !see the difference in the above 2 queiries 

-  associated roles and users
```
SELECT dp.name AS principal_name, dp.type_desc AS principal_type, 
       dr.name AS role_name
FROM sys.database_role_members drm
JOIN sys.database_principals dp ON drm.member_principal_id = dp.principal_id
JOIN sys.database_principals dr ON drm.role_principal_id = dr.principal_id;
```  

### Backup

Azure backup config is in the server section

- https://learn.microsoft.com/en-us/azure/azure-sql/database/automated-backups-overview?view=azuresql
- review these 2 [videos](https://www.youtube.com/watch?v=rbP7HSs3c4c&list=PLlrxD0HtieHi5c9-i_Dnxw9vxBY-TqaeN&index=46) the second provides details on restore
  - 1. determine time
  - 2. do a restore '-copy' (this puts it in the same server)
  - 3. query the -copy to confirm the restore has the correct data
  - 4. rename the bad db to '-old'
  - 5. rename '-copy' to orginal name
  - 6. confirm.
  - 7. delete '-old'

### Linking tables

```sql
-- Create Person table
CREATE TABLE Person
(
    PersonId INT IDENTITY PRIMARY KEY,
    FirstName NVARCHAR(128) NOT NULL,
    MiddelInitial NVARCHAR(10),
    LastName NVARCHAR(128) NOT NULL,
    DateOfBirth DATE NOT NULL
)

-- Create Student table
CREATE TABLE Student
(
    StudentId INT IDENTITY PRIMARY KEY,
    PersonId INT REFERENCES Person (PersonId),
    Email NVARCHAR(256)
)

-- Create Course table
CREATE TABLE Course
(
    CourseId INT IDENTITY PRIMARY KEY,
    Name NVARCHAR(50) NOT NULL,
    Teacher NVARCHAR(256) NOT NULL
)

-- Create Credit table
CREATE TABLE Credit
(
    StudentId INT REFERENCES Student (StudentId),
    CourseId INT REFERENCES Course (CourseId),
    Grade DECIMAL(5,2) CHECK (Grade <= 100.00),
    Attempt TINYINT,
    CONSTRAINT [UQ_studentgrades] UNIQUE CLUSTERED
    (
        StudentId, CourseId, Grade, Attempt
    )
)
```


- `CONSTRAINT [UQ_studentgrades] UNIQUE CLUSTERED`: This line defines a unique clustered index on the columns `StudentId`, `CourseId`, `Grade`, and `Attempt`. This means that the combination of these four columns must be unique in the table, and the data will be physically stored in the table in the order of this index.

--- 

# Examples

### Query for sessions

```sql
SELECT 
     SessionID = s.Session_id,
     resource_type,   
     DatabaseName = DB_NAME(resource_database_id),
     request_mode,
     request_type,
     login_time,
     host_name,
     program_name,
     client_interface_name,
     login_name,
     nt_domain,
     nt_user_name,
     s.status,
     last_request_start_time,
     last_request_end_time,
     s.logical_reads,
     s.reads,
     request_status,
     request_owner_type,
     objectid,
     dbid,
     a.number,
     a.encrypted ,
     a.blocking_session_id,
     a.text       

FROM   
     sys.dm_tran_locks l
     JOIN sys.dm_exec_sessions s ON l.request_session_id = s.session_id
     LEFT JOIN   
     (
         SELECT  *
         FROM    sys.dm_exec_requests r
         CROSS APPLY sys.dm_exec_sql_text(sql_handle)
     ) a ON s.session_id = a.session_id 
```