---
---


M$ says they are bring gui to wsl2 later in the year (find the ref)

### Confirm you're running WSL2 linux versions
   * wsl2 -l -v

### KALI gui   
WIP pulled from: www.youtube.com/watch?v=oTD8cYluUgk
   * Pull kali from the Microsoft store
   * sudo apt update && sudo apt -y upgrade
   * sudo apt install -y xfce4
   * sudo apt install xrdp
   * sudo /etc/init.d/xrdp start

   * connect with windows 'remote desktop connection'   using localhost 3389
    login with your linux credientials


### UBUNTU using a lightweight gui
WIP: remove.   Pulled from https://www.youtube.com/watch?v=IL7Jd9rjgrM
   * sudo apt update && sudo apt -y upgrade
   * sudo apt install xrdp
   * sudo apt install -y xfce4  # a lightweight graphical interface
   * sudo apt install -y xfce4-goodies   # 
   * sudo cp /etc/xrdp/xrdp.ini  /etc/xrdp/xrdp.ini.bak
   * sudo sed -i 's/3389/3390/g' /etc/xrdp/xrdp.ini   # change port to 3390
   * sudo sed -i 's/max_bpp=24/#max_bpp=24\nmax_bpp=128/g' /etc/xrdp/xrdp.ini   # inc performance for a local gui
   * sudo sed -i 's/xserverbpp=24/#xserverbpp=24\nxserverbpp=128/g' /etc/xrdp/xrdp.ini
   * echo xfce4-session > ~.xsession
   * sudo nano /etc/xrdp/startwm.sh
     -   hash out #   last two lines  (test & exec cmds)
     -   at the end, add:  startxfce4 
     -   save and exit
   * sudo /etc/init.d/xrdp start

   * connect with windows 'remote desktop connection'   using localhost 3390
    login with your linux credientials 
    




