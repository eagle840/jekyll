# Azure Functions

## Development Environment


- vsc
- Azure Functions Core Tools [link](https://learn.microsoft.com/en-us/azure/azure-functions/functions-run-local?tabs=windows%2Cisolated-process%2Cnode-v4%2Cpython-v2%2Chttp-trigger%2Ccontainer-apps&pivots=programming-language-csharp)
- Azure Functions VSC extensio
- Azure Storage Emulator
- Azure CosmosDB Emulator
- CAN THIS BE BUNDLED IINTO DEVELOPEMENT CONTAINERS (below, chat3.5 generated, needs testing) devcontainer.json

```
{
    "name": "Azure Dev Container",
    "dockerFile": "Dockerfile",
    "settings": {
        "terminal.integrated.shell.linux": "/bin/bash"
    },
    "extensions": [
        "ms-azuretools.vscode-azurefunctions"
    ],
    "forwardPorts": [7071, 7072], // Ports used by Azure Functions
    "postCreateCommand": "az extension add --name storage-preview", // Install Azure Storage Emulator extension
    "postStartCommand": "azurite --silent --location /data && /opt/cosmosdb/Emulator/AccountBridge /data", // Start Azure Storage Emulator and CosmosDB Emulator
    "remoteUser": "vscode",
    "workspaceMount": "source=/path/to/your/project/folder,target=/workspace,type=bind,consistency=cached"
}
```

## Functions

### Functions command

Install Core Tool (Azure 'local' Functions)

recommended for use on c# or JS

`func init` -> `func new` -> `func start`

- create a folder, then:
- func init # creates /host.json
- func new # creates /(funcname)>/function.json + code
- func start # starts func app locally
- func azure functionapp publish  <az apname> #(log into az 1st)

- WESITE_CONTENTAZUREFUNCTIONCONNECTIONSTRING # funcapp setting for azure storage for the func app

## Durable Function

Three parts
- Client (starter) function: starts a new orchestration, and uses any trigger
- Orchestrator Function: defines steps (activity f()) work flow and error handling
- Activity F(): can use any bindings

You'll need an sdk

### patterns

- chaining
- fan-out and fan-in
- Asynchronous HTTP APIs: for polling progress of an orchestration
- Monitor:  Poll and sleep
- Human interaction

## Custom Handers


- Use your own runtime
- You use the 'Functions Host' to send and recieve http traffice from your run time.
- The Functions Host takes care of the bindings and triggers
- Example docs for Go/Rust [MS Docs](https://learn.microsoft.com/en-us/azure/azure-functions/create-first-function-vs-code-other?tabs=go%2Cwindows)

## logging

connect to the app scm and check the 
- logfiles/application/functions/function  and  /host

## files

- host # /host.jon
- func # /<funcName>/function.json

## triggers

### http

- types: anonymous, f(), admin


