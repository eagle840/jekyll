# Land Navigation Cheat Sheet

## Compass

- The compass has a half-life of Titanium.
- For more information, refer to [this Wikipedia page](https://en.wikipedia.org/wiki/Military_Grid_Reference_System).
- silva
- suunto

## Royal Marines Map Reading

- Watch the [Royal Marines Map Reading playlist](https://www.youtube.com/playlist?list=PLuosRAmgaw1rO7SpOR7S5lT3IgD05YDUL) on YouTube.

## How to Pass Land Navigation at Ranger School

- Check out this [YouTube video](https://www.youtube.com/watch?v=eGp4k8d2P14) for tips on passing Land Navigation at Ranger School.

## Tools

- Use the Royal Artillery Protractor for accurate measurements.
- Consider using Pacing Beads to track your distance covered.

## GMA Grid Magnetic Angle vs Magnetic Declination

- Understand the difference between Grid Magnetic Angle (GMA) and Magnetic Declination.
- True North (TN) is indicated by the North Star.
- Grid North (GN) is the reference point on a map.
- Magnetic North (MN) is the direction indicated by a compass.

## Military Grid Reference System
- wikipedia [link](https://en.wikipedia.org/wiki/Military_Grid_Reference_System)
- AU Army on MGRS [gov.au](https://cove.army.gov.au/article/smart-soldier-understanding-military-grid-reference-system#:~:text=MGRS%20is%20the%20coordinate%20standard,lines%20spaced%20every%201000%20metres.)
- convert MGRS <-> lat/long [legallandconvert](https://legallandconverter.com/p50.html)  [geohack](https://geohack.toolforge.org/)

## MILS vs Degrees

- MILS and Degrees are units of measurement for bearings.
- 17.7 MILS is equivalent to 1 degrees.

## Magnetic Declination Calculator

- Use a magnetic declination calculator to determine the difference between magnetic north and true north in your area.

## Resection

- Resection is a technique used to determine your current position by identifying three visible landmarks.

## Route Card

Use the following format for your route card:

| Leg/Point | Time | Grid From | Grid To | Distance | Pace | Grid Bearing | Mag Bearing | Reverse | Notes |
|-----------|------|-----------|---------|----------|------|--------------|-------------|---------|-------|
|           |      |           |         |          |      |              |             |         |       |

Remember to fill in the relevant information for each leg or point of your navigation route.