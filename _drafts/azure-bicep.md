# Azure Bicep

- A domain-specific language (DSL) and a tanspiler (like typescript is to js), Bicep into JSON
- Incs loops and conditionals
- arm -> <decomplier> -> bicep
- It's declarative (CLI and P$ are Imperative)
- CLI and PS$ version accept bicep
- The cmd 'bicep build main.bicep' will show the JSON ARM template
- decompile (yes) ARM into Bicep 'bicep decompile'
- See the bicep playground to compare Bicep and ARM JSON  [Bicep Playground](https://azure.github.io/bicep/)


## Training

- [MS video Intro 30 min intro into using bicep](https://www.youtube.com/watch?v=sc1kJfcRQgY)
- [MS Learn](https://learn.microsoft.com/en-us/training/paths/fundamentals-bicep/)

## Example

keyword resource_indentifier  'resource_type@apiVersion' = { <object> }

```
resource appPlan 'Microsoft.Web/serverfarms@2020-06-01' = {
	name: 'appPlan'
	location: resourceGroup() .location
	kind: 'linux'
	sku: {
		name: 'B1'

	}
	properties: {
		reserved: true

	}
}
```

```
param location string = resourceGroup().location
param namePrefix string = 'storage'

var storageAccountName = '${namePrefix}${uniqueString(resourceGroup().id)}'
var storageAccountSku = 'Standard_RAGRS'

resource storageAccount 'Microsoft.Storage/storageAccounts@2023-05-01' = {
  name: storageAccountName
  location: location
  kind: 'StorageV2'
  sku: {
    name: storageAccountSku
  }
  properties: {
    accessTier: 'Hot'
    supportsHttpsTrafficOnly: true
  }
}

output storageAccountId string = storageAccount.id
```

run: 'bicep build .\filename.bicep' # generates a filename.json arm template