# Web Application firewalls


## one

## demos

https://www.openappsec.io/playground  (need login)

https://owasp.org/www-project-juice-shop/

## 'Core Rule Sets'

https://owasp.org/www-project-modsecurity-core-rule-set/

Here are some open-source Web Application Firewalls (WAFs) that support the Core Rule Set (CRS):

1. [OWASP ModSecurity](https://github.com/owasp-modsecurity/ModSecurity): ModSecurity is the standard open-source web application firewall (WAF) engine. It is free software released under the Apache license 2.0 and remains the most widespread open source web application firewall engine¹. The OWASP ModSecurity project provides the WAF engine¹. It is usually coupled with OWASP CRS, that brings protection against HTTP attacks¹.
2. [OWASP Coraza WAF](https://coraza.io/): The OWASP Coraza WAF project is a WAF framework that can be easily integrated into your applications. It supports the OWASP ModSecurity CRS rules and Modsecurity syntax⁴.
3. [NAXSI](https://github.com/nbs-system/naxsi): NAXSI is an open-source, high performance, low rules maintenance WAF for NGINX⁵.


Please note that the above list is not exhaustive and there may be other open-source WAFs that also support CRS. Always check the official documentation of the WAF for the most accurate and up-to-date information.

