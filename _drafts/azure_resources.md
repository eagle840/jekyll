---
---

# List of resourses for operations and developers

## Azure Cloud Framework   
  * https://docs.microsoft.com/en-us/azure/cloud-adoption-framework/
  * https://docs.microsoft.com/en-us/azure/architecture/
  

## Azure Status
    * twitter.com/azurestatus
    * azure.microsoft.com/en-us/status  # support > dashboard
    * setup an azure dashboard in azure portal
    * ? azure monitor > service health
    
## ARM Templates in Action
    * create your template.json
    * create your parameterfile.parameters.json
    * http://armviz.io/designer   # generate online
    * or your own generator with: https://docs.microsoft.com/en-us/azure-stack/user/azure-stack-validate-templates?view=azs-2002
    * deploy with PS  
    * New-AzResourceGroupDeployement - resourceGroupName RG-IaCSample `
        -TemplateFile "template.json" -TemplateParameterFile "parameterfile.parameters.json"
    * remember, update the files and re-deploy, and the resource will be updated.
    * In azure dev pipeline, use a 'Azure Resource group deployment' to deploy templete
    
## Azure Web Apps
  * kudu [github](https://github.com/projectkudu/kudu) [docs](https://learn.microsoft.com/en-us/azure/app-service/resources-kudu)



## Azure Application Insight.

Three ways to access
- Monitor page -> applicaton insights blade
- In web app blade
- It's own resource, usually created in same RG as it's monitoring

Backed by
- LAWS Log Analytics WorkSpace
- Configured in diagnostic Settings

Learn
- <https://learn.microsoft.com/en-us/training/modules/monitor-app-performance/?source=recommendations>

## Other

Azure resource list of providers and subscriptions  https://resources.azure.com/

Azure security tools list    
  * https://secdevtools.azurewebsites.net/
  * https://gallery.technet.microsoft.com/ # look for security (ASC) playbooks


Azure tenant lookup (by name)
  - https://gettenantpartitionweb.azurewebsites.net/
  
WHOLE BUNCH OF STUFF MISSING DUE TO A WINDOWS UPDATE RESTART - Thank M$

VS CODE Powershell   extensions
  *  powershell
  *  Azure Resourse manager tools
  *  Azure resourse manager snippets
  *  Azure Account
   

Other tools   
   * https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15
   * http://aka.ms/SecurityWebinars
   * https://aad.portal.azure.com/#allservices
   * https://endpoint.microsoft.com/#home
   * https://resources.azure.com/  #be logged in, and them select your subscription
   * https://microsoft.github.io/AzureTipsAndTricks/