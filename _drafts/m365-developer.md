# M365 Developer

## Main sites

- Admin portal [admin](https://admin.microsoft.com/Adminportal/Home#/homepage)

## Secondary Sites

- Office 365 Admin Center https://admin.microsoft.com
- Office 365 Security & Compliance Center   https://protection.office.com/unifiedauditlog
- Azure Active Directory https://portal.azure.com
- Exchange Admin Center  https://outlook.office365.com/ecp/?rfr=Admin_o365
- Exchange Distribution Groups https://admin.cloud.microsoft/exchange?page=groups#/
- SharePoint Admin Center  https://admin.microsoft.com/sharepoint?page=home&modern=true
- Office 365 Compliance Center  https://compliance.microsoft.com/
- Office 365 Security Center  https://security.microsoft.com
- Endpoint / Device Management (Intune)   https://devicemanagement.microsoft.com/
- Cloud App Security   https://portal.cloudappsecurity.com/
- Teams Admin Center   https://admin.teams.microsoft.com/
- All Office 365 Admin Centers  https://admin.microsoft.com/AdminPortal/Home#/alladmincenters
- 354 Grpah explorer https://developer.microsoft.com/en-us/graph/graph-explorer

## Groups (Office365/Security)

Here's a breakdown of the differences between **Office 365 Groups** and **Security Groups** in Microsoft 365:

### Office 365 Groups
- **Purpose**: Designed for collaboration and communication among users.
- **Resources**: Comes with shared resources like a group email inbox, calendar, document library (SharePoint), OneNote notebook, Planner, and more.
- **Management**: Managed through the Microsoft 365 Admin Center.
- **Membership**: Can include users from both inside and outside your organization (if allowed by the admin).
- **Usage**: Ideal for project teams, departments, or any group needing to collaborate on documents, share emails, and manage tasks together¹(https://learn.microsoft.com/en-us/microsoft-365/admin/create-groups/compare-groups?view=o365-worldwide).
- **Grah**: these are called **unified groups**

### Security Groups
- **Purpose**: Primarily used for managing access to resources and permissions.
- **Resources**: Does not come with shared resources like mailboxes or document libraries.
- **Management**: Managed through Azure Active Directory (Azure AD).
- **Membership**: Can include users, devices, other security groups, and service principals.
- **Usage**: Used to grant access to resources such as SharePoint sites, apply policies, and assign licenses²(https://learn.microsoft.com/en-us/answers/questions/732613/azure-ad-what-is-difference-between-security-group).

### Dynamic Groups
- **Purpose**: Automates group membership based on user or device attributes.
- **Resources**: Can be used with both Office 365 Groups and Security Groups.
- **Management**: Managed through Azure Active Directory (Azure AD).
- **Membership**: Automatically updated based on defined rules and attributes (e.g., department, location, job title).
- **Usage**: Ideal for scenarios where group membership needs to be dynamically maintained without manual intervention²(https://techcommunity.microsoft.com/blog/itopstalkblog/dynamic-groups-in-azure-ad-and-microsoft-365/2267494)³(https://learn.microsoft.com/en-us/entra/identity/users/groups-dynamic-membership).

In summary, **Office 365 Groups** enhance collaboration with shared tools and resources, **Security Groups** focus on access management and permissions, and **Dynamic Groups** automate membership updates based on specific attributes.

### 'Teamifing' a Group
- converting a Office365 group into a 'Microsoft Teams' Team.

Source: Conversation with Copilot, 07/02/2025
(1) Compare types of groups in Microsoft 365 - Microsoft 365 admin. https://learn.microsoft.com/en-us/microsoft-365/admin/create-groups/compare-groups?view=o365-worldwide.
(2) Azure AD ... what is difference between security group and microsoft365 .... https://learn.microsoft.com/en-us/answers/questions/732613/azure-ad-what-is-difference-between-security-group.

## Office365 Groups vs MS Teams team


Here's a comparison between **Office 365 Groups** and **Microsoft Teams** teams, along with an explanation of "teamifying":

### Office 365 Groups
- **Purpose**: Designed for collaboration and communication among users.
- **Resources**: Comes with shared resources like a group email inbox, calendar, document library (SharePoint), OneNote notebook, Planner, and more.
- **Management**: Managed through the Microsoft 365 Admin Center.
- **Membership**: Can include users from both inside and outside your organization (if allowed by the admin).
- **Usage**: Ideal for project teams, departments, or any group needing to collaborate on documents, share emails, and manage tasks together¹(https://learn.microsoft.com/en-us/microsoftteams/office-365-groups).

### Microsoft Teams Teams
- **Purpose**: Built on top of Office 365 Groups, Teams provides a more integrated and interactive collaboration experience.
- **Resources**: Includes all the resources of an Office 365 Group, plus additional features like chat, video conferencing, and integration with third-party apps.
- **Management**: Managed through the Microsoft Teams Admin Center.
- **Membership**: Uses the same membership as the underlying Office 365 Group.
- **Usage**: Ideal for real-time communication and collaboration, with features like channels for different topics or projects, and the ability to integrate with various apps and services¹(https://learn.microsoft.com/en-us/microsoftteams/office-365-groups)²(https://askanydifference.com/difference-between-microsoft-groups-and-microsoft-teams-with-table/).

### Teamifying
- **Definition**: "Teamifying" refers to the process of converting an existing Office 365 Group into a Microsoft Teams team.
- **Purpose**: Enhances the collaboration capabilities of an Office 365 Group by adding the real-time communication and additional features provided by Microsoft Teams.
- **Process**: When you teamify an Office 365 Group, you essentially attach a new Teams instance to it, allowing the group to use channels, chat, video calls, and other Teams functionalities³(https://www.cogitohive.tech/2021/01/22/should-i-teamify-my-sharepoint-site-things-to-consider/)⁴(https://reshmeeauckloo.com/posts/powershell_teamifym365group/).

In summary, while **Office 365 Groups** provide a solid foundation for collaboration with shared resources, **Microsoft Teams** teams build on this by adding real-time communication and more interactive features. **Teamifying** an Office 365 Group allows you to leverage the full capabilities of Microsoft Teams for enhanced collaboration.

Do you have any specific scenarios where you're considering using these tools?

Source: Conversation with Copilot, 07/02/2025
(1) Microsoft 365 Groups and Microsoft Teams - Microsoft Teams. https://learn.microsoft.com/en-us/microsoftteams/office-365-groups.
(2) Microsoft Groups vs Microsoft Teams: Difference and Comparison. https://askanydifference.com/difference-between-microsoft-groups-and-microsoft-teams-with-table/.
(3) Should I “Teamify” my SharePoint Site? Things to Consider. https://www.cogitohive.tech/2021/01/22/should-i-teamify-my-sharepoint-site-things-to-consider/.
(4) Teamifying an Existing M365 Group with PowerShell. https://reshmeeauckloo.com/posts/powershell_teamifym365group/.

## API simulator

[Dev Proxy](https://learn.microsoft.com/en-gb/microsoft-cloud/dev/dev-proxy/overview) is an API simulator that helps you effortlessly test your app beyond the happy path.

## VSC Live Server

This extension appears to be used alot [link](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)


## Graph Toolkit (msgtk)

Starting with MS graph course https://learn.microsoft.com/en-gb/training/paths/m365-msgraph-toolkit/

You can use [Graphtool kit](https://learn.microsoft.com/en-us/graph/toolkit/overview?tabs=html)

Graph Toolkit uses [Fluent UI](https://developer.microsoft.com/en-us/fluentui#/), ie JavaScript (react)


??? https://mgt.dev/  only works in edge?


web apps without using a bundler,
- ```<script src="https://unpkg.com/@microsoft/mgt@3/dist/bundle/mgt-loader.js"></script>```

npm 
- ```npm install @microsoft/mgt```

react
- ```npm install @microsoft/mgt-react```

Login component in your application - see [mgt.dev](https://mgt.dev/) for a interactive playground (Edge only)
- ```<mgt-login></mgt-login>```

### Providers in msgtk

- Microsoft Authentication Library (MSAL) v2 provider: Recommended for use by single-page applications that authenticate from the browser. Uses the OAuth Authorization Code Flow with PKCE that's considered more secure than the OAuth2 implicit flow.
- SharePoint provider: For use in the SharePoint Framework.
- Microsoft TeamsFx provider: For use by single-page applications that run as Teams tabs.
- Electron provider: For use in Electron apps.
- Proxy provider: For use by web applications that authenticate on the server.
- Custom provider: Allows you to build your own authentication.