# Terraform Cheat Sheet


## Workflow

 - init
 - fmt
 - validate
 - plan [-replace (if you just want a single resouce replaced) -destory, taint, ]
 - terraform plan -refresh-only # use instead of terraform refresh
 - apply
 - !NOT refresh, use plan, apply -refresh
 - destroy

## Varibles https://www.terraform.io/language/values/variables
 
  - variables are considered inputs to the code, eg local variables, variable files etc  var.name
  - variables should be declared ?  variable "name" { type:x   default:y  description:z }
  - locals for considered variables inside the files, are aren't altered my input variables   local.name
  - locals declared in a locals { }  block
  - variables and locals are not accessable outside a module, you have to use output  {} to get at them
 
###Variable hiacheire (https://www.terraform.io/language/values/variables#variable-definition-precedence)

 1. default
 2. ?local
 3. Environment variables, export TF_VAR_image_id=
 4. The terraform.tfvars file, if present.
 5. The terraform.tfvars.json file, if present.
 6. Any *.auto.tfvars or *.auto.tfvars.json files, processed in lexical order of their filenames.
 7. Any -var and -var-file: '-var="k=v", '-var-files="filename"

### variable workflow

 - var.tf to define varibles
 - terraform.tfvars  to store varible values, then gitignore  *.tfvars


 ## Providers

 ### terraform


terraform {
  required_version = ">= 1.1.0" # to spec a tf version, "->"  right most version can increment

  required_providers {
    http = {
      source = "hashicorp/http"
      version = ">= 2.1.0"
    }
  }
}

provider "http" {}   # similar to wget, curl  ?? why not in the 'required providers' ??? why is it repeated here

### other

#### cloud providers

#### special
 - random 
 - http
 - local    https://registry.terraform.io/providers/hashicorp/local/latest/docs


## data

data "http" "example" {
  url = "https://www.hashicorp.com/"
}

### 


### Loading local data

#### json files

```sh
ubuntu $ cat main.tf 
locals {
  # Load all of the data from json
  all_json_data = jsondecode(file("config_data.json"))
  ```

## Provisioners https://www.terraform.io/language/resources/provisioners/syntax

Provisioners are usually tied to a resourse

 ### local

 resource "local_file" "foo" {
  content  = tls_private_key.ssh_key.private_key_pem
  filename = "key.pem"
  file_permission = "0400"
}

For example, add this to the ec2 instance resource block, to get a txt file with the ec2 ip

  provisioner "local-exec" {
    command = "echo ${self.private_ip} >> private_ips.txt"
  }

### remote-exec

again, it will be inside a resource block (eg ec2_instance), but will run on the remove vm

provisioner "remote-exec" {
  inline = [
    "echo ${self.private_ip} >> null_resource.txt",
  ]
}

### null

Run a provisioner without associated resource

- note how this one is triggered and connected

resource "null_resource" "web" {
  triggers = {
    id = aws_instance.web.id
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = tls_private_key.ssh_key.private_key_pem
    host        = aws_instance.web.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "echo ${aws_instance.web.private_ip} >> null_resource.txt",
    ]
  }
}


## Expressions

### reserved varibles

#### count

 - count is a reserved int variable used in a resource block, to create that number of resources
 - naming convension is too add [n] to the resource name

#### conditional

  - condition ? true_val : false_val
  - (if this condition is true) ? (do this) : (else do this)
  - EG count = var.provision_ec2 == true ? var.ec2_count : 0  
  -  if ((var.provision_ec2 == true) is true) use ec2_count : else 0

#### for  https://www.terraform.io/language/expressions/for
  - A for expression creates a complex type value by transforming another complex type value. Each element in the input value can correspond to either one or zero values in the result, and an arbitrary expression can be used to transform each input element into an output element.
  - [ for i in json.data.list : i]
  - note brackets for list, returns i for each item in list, and outputs list[i]
  - for key,value in resource.map : key => lower(value) 
  { for  i,k  in local.all_json_data.Map1 : i=>k  }  # notice the curlys - a mapping
  - [ for i in local.all_json_data.List3 : i if  i.Food  == "Taco" ]
  - code
  ```sh
  variable "ec2_tags" {
  type        = map(string)
  default     = {
    Provisioned_With = "TeRRaFoRM",
    Type             = "web",
    Type_2           = "API"
  }
  description = "A map of tags for the EC2 instance(s)"
}```
  - code 
  ```sh
  tags = {
    for key, value in var.ec2_tags :
    key => lower(value)
  }
  ```

#### for_each

 - If a resource or module block includes a for_each argument whose value is a map or a set of strings, Terraform will create one instance for each member of that map or set.
 - code
 ```sh
variable "host_type_list" {
  type        = list(string)
  default     = ["t3.micro", "t3.small"]
  description = "A list of host types"
}
 ```
  - code
```sh
resource "aws_instance" "ec2" {
  for_each      = toset(var.host_type_list)
  ami           = data.aws_ami.ubuntu.id
  instance_type = each.key
  ```

#### Dynamic blocks

  - A dynamic block acts much like a for expression, but produces nested blocks instead of a complex typed value.
  - code
  ```sh
  variable "block_device" {
  type        = list(string)
  default     = ["sdf","sdg"]
  description = "A list of block device names."
 }
  ```
  - code
  ```sh
    resource "aws_instance" "ec2" {
  for_each      = toset(var.host_type_list)
  ami           = data.aws_ami.ubuntu.id
  instance_type = each.key

  dynamic "ebs_block_device" {
    for_each = var.block_device
    content {
      device_name = ebs_block_device.value
      volume_size = 1
    }
  }
  ```

## Functions

  - value = replace(random_pet.server_name.id, "-", " ")
  - quote = format("Terraform is %s!", var.adjective)
  - zipmap   https://www.terraform.io/language/functions/zipmap

  #### Lookup Function

  variable "name" {
  type        = string
  description = "internal name of server"
}


variable "map_of_ami" {
  type = map
  default = {
    dev-server = "ami-0977f0ee55df07012"
    prd-server = "ami-08d6b202c2f085f37"
    db-server  = "ami-011da27da18f4d8df"
  }
}


output "ami" {
  value = lookup(var.map_of_ami, var.name)
}



## console

### tricks
  - lists need to end with ... EG max([12, 54, 3, 27, 04, 87]...)

## Modules

 - providers are to be in the root directry, not module (?) Make sure you put providers in documentation


 ## Terraform cloud

 ### cli

  - terraform login  # will generate a new api token, using a redirect to a webpage
  - linux /Users/<user>/.terraform.d/credentials.tfrc.json
  - win   C:\Users\<user>\AppData\Roaming\terraform.d\credentials.tfrc.json
  - example
'''json
{
  "credentials": {
    "app.terraform.io": {
      "token": "token_value"
    }
  }
}
'''
 ---------------------------------


 # AWS

 provider.tf
 ===========

 provider "aws" {
  region = "us-east-2"
}

provider "tls" {}  # allow to create a ssh 

ec2.tf
======

create keys as well



# Generate the SSH key pair
resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Upload the public key to AWS
resource "aws_key_pair" "ssh_key_pair" {
  key_name   = "ssh_key"
  public_key = tls_private_key.ssh_key.public_key_openssh
}

#create local key file using a local provisioner
resource "local_file" "foo" {
  content  = tls_private_key.ssh_key.private_key_pem
  filename = "key.pem"
  file_permission = "0400"
}


# create ec2 instance
resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"

  subnet_id              = aws_subnet.lab_subnet.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]

  key_name = aws_key_pair.ssh_key_pair.key_name

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = tls_private_key.ssh_key.private_key_pem
    host        = self.public_ip
  }

  provisioner "file" {
    source      = "helloworld.txt"
    destination = "/tmp/helloworld.txt"
  }
}

networking.tf
=============

# networking.tf

# Create a VPC
resource "aws_vpc" "lab_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "lab_vpc"
  }
}

# Create a subnet inside VPC
resource "aws_subnet" "lab_subnet" {
  vpc_id                  = aws_vpc.lab_vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name = "lab_vpc"
  }
}

# Create Security Group for SSH
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic; Allow all outbound traffic"
  vpc_id      = aws_vpc.lab_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create Internet Gateway for VPC
resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.lab_vpc.id
}

# Create default route table for VPC
resource "aws_default_route_table" "route_table" {
  default_route_table_id = aws_vpc.lab_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default.id
  }

  tags = {
    Name = "Default route table"
  }
}


