# setup a vpn server to surf the world

using:   
How to Setup a VPN Server and Connect Through It   
https://www.youtube.com/watch?v=CBJMl9MILbg   

## setup a ubuntu server in the area   

- update and upgrade, reboot   
- sudo apt install openvpn   
- git clone https://github.com/Angristan/OpenVPN-install   
- cd OpenVPN-install   
- chmod +x openvpn-install.sh   
- sudo ./openvpn-install.sh   
- if you get a 'tun not available'   
    * cd /dev   
    * mkdir net    
    * mknod net/tun c 10 200   
    * chmod 0666 /dev/net/tun   
- should have a client.ovpn in the folder you run the sh in   

## linux client   

- install openvpn   
- copy client.opvn from server (using scp)   
- copy to /etc/openvpn/client/   
- run openvpn client.ovpn  
- should get an ip  
- curl ipconfig.me  
- exit the running openvpn   
- curl ipconfig.me  
- consider runnning   
-  * systemctl enable openvpn   
-  * use a network manager  apt install network-manager-openvpn   
-  * not forget to import the client.ovpn file    

## Windows

- openvpn.net/community-downloads/   
- install   
- open client.ovpn   
- copy to /pf/Openvpn/client  
- check video for further setting if needed



