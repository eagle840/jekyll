---
layout: page
title: Large Language Models
permalink: /LLM/
---

Large Language Models - cheatsheet

Quicklinks
- Chat with youtube video (with huggingface account) [link](https://huggingface.co/spaces/Illia56/Chat-with-Youtube-video-Llama-70b)

## LLM Generation

Creating a Large Language Model (LLM) involves several critical steps to ensure its successful development and alignment with intended applications and ethical standards. Here’s an overview of the key stages involved in this process:

**1 Define the Use Case:**

Clearly articulate the purpose of adopting an LLM. Identify the specific problem or task you want the model to address. Consider whether building your own LLM or collaborating with a partner is the best approach1.

**2 Data Collection:**

Gather relevant and high-quality data. The success of an LLM heavily depends on the quality and diversity of the training data. Collect text data from various sources, ensuring it covers the domain or context you intend to model.

**3 Data Preprocessing:**

Clean and preprocess the collected data. This step involves removing noise, handling missing values, tokenizing text, and converting it into a suitable format for training. Proper preprocessing ensures that the LLM learns effectively.

**4 Model Architecture Selection:**

Choose an appropriate architecture for your LLM. The most popular choice is the transformer-based architecture, which has revolutionized natural language processing (NLP). Examples include GPT (Generative Pre-trained Transformer) models and BERT (Bidirectional Encoder Representations from Transformers).
Transformers excel at capturing long-range dependencies in text, making them ideal for language modeling tasks.

**5 Hyperparameter Tuning:**

Define hyperparameters that govern the behavior of your LLM. These include:
- Number of Layers: Transformers consist of multiple layers (e.g., 12, 24, or more). Deeper models can capture complex patterns but may require more computational resources.
- Attention Heads: Transformers use multi-head attention mechanisms. The number of attention heads affects the model’s ability to attend to different parts of the input.
- Hidden Units (Dimensionality): The size of the hidden layers impacts the model’s capacity to learn.
Learning Rate: Determines how quickly the model adapts during training.
- Batch Size: Balancing efficiency and convergence speed.
- Dropout Rate: Regularization technique to prevent overfitting.

Hyperparameter tuning involves experimentation and validation to find optimal values.

**6 Pretraining and Fine-Tuning:**

Pretrain the LLM on a large corpus of text data. During pretraining, the model learns to predict missing words (masked language modeling) or generate coherent text.

Fine-tune the pretrained model on specific downstream tasks (e.g., sentiment analysis, question answering). Fine-tuning adapts the model to the target domain.


**7 Training Data and Objective Function:**

Curate a diverse and representative dataset for training. Include a wide range of text genres, topics, and styles.   

Define the objective function (loss function) for training. Common choices include cross-entropy loss or mean squared error, depending on the task.

**8 Training Process:**

Use powerful GPUs or TPUs for efficient training.
Monitor training progress using validation data. Early stopping can prevent overfitting.
Save checkpoints during training to resume or evaluate later.

**9 Evaluation Metrics:**

Assess the LLM’s performance using metrics relevant to your use case. Common NLP evaluation metrics include perplexity, accuracy, and F1-score.

**10 Ethical Considerations:**

Ensure fairness, transparency, and bias mitigation during model development.
Regularly audit the LLM for unintended biases or harmful outputs.


## OpenAI
a collection of instructional labs, 

- OpenAI api [Ref](https://platform.openai.com/docs/api-reference/introduction)
- OpenAI cookbook [Cook Book](https://github.com/openai/openai-cookbook/)

## Ollama


- [ollama.ai](https://ollama.ai/)
- [ollama on github](https://github.com/jmorganca/ollama)
- Getting started with ollama localy [youtube](https://www.youtube.com/watch?v=Ox8hhpgrUi0)


## Microsoft Autogen
- <https://github.com/microsoft/autogen>
- [AutoGen Docs](https://microsoft.github.io/autogen/)
- [AutoGen examples](https://microsoft.github.io/autogen/docs/Examples/AutoGen-AgentChat/)

## Langchain
Framework for developing applications powered by language models
- [Concepts](https://docs.langchain.com/docs/)
- [Python Docs](https://python.langchain.com/docs/get_started)
- [API reference](https://api.python.langchain.com/en/latest/api_reference.html)
- [LangFlow](https://github.com/logspace-ai/langflow)

## llamaindex
- [www.llamaindex.ai](https://www.llamaindex.ai/)

## LLM Local
- [LM Studio](https://lmstudio.ai/)
- [LLMAnything](https://useanything.com)

## ONNX
- [ONNX](https://onnx.ai/)

## LLaVA: Large Language and Vision Assistant
- [github](https://llava-vl.github.io/)
- [Microsoft LLaVA](https://www.microsoft.com/en-us/research/project/llava-large-language-and-vision-assistant/)

## Learning/Labs
- [deeplearning.ai](https://www.deeplearning.ai/short-courses/)
- [HuggingFace NLP](https://huggingface.co/learn/nlp-course/chapter1/3?fw=pt)
- [Prompt Engineering for ChatGPT](https://www.coursera.org/learn/prompt-engineering/)



## Vectors and Embedding

###  learning

- <https://kdb.ai/learning-hub/fundamentals/vector-embeddings/>
- <https://learn.microsoft.com/en-us/semantic-kernel/memories/vector-db>
- <https://weaviate.io/blog/what-is-a-vector-database>


### vector storage/databases

- <https://kdb.ai/>
- <https://www.trychroma.com/>
- FB faiss [github](https://github.com/facebookresearch/faiss)  [intro](https://engineering.fb.com/2017/03/29/data-infrastructure/faiss-a-library-for-efficient-similarity-search/)
- <https://weaviate.io/>
- <https://www.pinecone.io/>

## Other Tools

- Microsoft [Semantic-Kernel][https://learn.microsoft.com/en-us/semantic-kernel/overview/]
- https://openinterpreter.com/ 
- [gpt engineer](https://github.com/AntonOsika/gpt-engineer)
- https://haystack.deepset.ai/
- [Azure Machine Learning Exercises](https://microsoftlearning.github.io/mslearn-dp100/)
- [gpt4all](https://docs.gpt4all.io/) [repo](https://github.com/nomic-ai/gpt4all/tree/main)
- Getting started on ML on dotnet [link](https://github.com/dotnet/csharp-notebooks?WT.mc_id=dotnet-35129-website#machine-learning)

## V-Blogs for LLM's and AI

- https://www.youtube.com/@AllAboutAI
- https://www.youtube.com/@matthew_berman

## Leader boards

- [Open LLM Leaderboard](https://huggingface.co/spaces/HuggingFaceH4/open_llm_leaderboard)
- [LMSYS Chatbot Arena Leaderboard](https://huggingface.co/spaces/lmsys/chatbot-arena-leaderboard)
