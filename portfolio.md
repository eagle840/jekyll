---
layout: page
title: Portfolio
permalink: /portfolio/
---

The IR4 Engineers Portfolio


## Katacoda
a collection of instructional labs, 

- K8s Setup a Calico networking solution on Kubenetes:  [www.katacoda.com/ir4engineer/scenarios/calico](https://www.katacoda.com/ir4engineer/scenarios/calico)
- Rapidly start up tensorflow/juypter with docker: [www.katacoda.com/ir4engineer/scenarios/dockertensor](https://www.katacoda.com/ir4engineer/scenarios/dockertensor)
- Mongodb administration: [www.katacoda.com/ir4engineer/scenarios/mongo30twomins](https://www.katacoda.com/ir4engineer/scenarios/mongo30twomins)
- SQL administration: [www.katacoda.com/ir4engineer/scenarios/sql](https://www.katacoda.com/ir4engineer/scenarios/sql)
- Setup a standalone Spark server:  [www.katacoda.com/ir4engineer/scenarios/singlespark](https://www.katacoda.com/ir4engineer/scenarios/singlespark)
- Linux Network troubleshooting: [www.katacoda.com/ir4engineer/scenarios/tcpdumpnc](https://www.katacoda.com/ir4engineer/scenarios/tcpdumpnc)


## Azure ARM template for a Kubernetes server
Deployed  k8s server
- A deployed  k8s server by triggering a deployment on Azure Devops Pipelines
- [52.165.160.43/index.html](http://52.165.160.43/index.html)

## Apps
- Simple nodejs fullstack with Mongo type database and bootstrap for tracking job applications, running on a docker container in Azure. [ir4jobhunt.azurewebsites.net](https://ir4jobhunt.azurewebsites.net/)
- A SPA being pulling from AWS S3, pulling data from a Neo4j database, showing UK security structure  http://uksecurity.ir4engineer.com/index.html


## My Course Syllabus

 - Read sabatical Syllabus covered over 2019 and 2020  at [eagle840.gitlab.io/jekyll/mydegree/](https://eagle840.gitlab.io/jekyll/mydegree/)


## Other CI/CD  
 - My Blog is run that the Gitlabs pipelines using Jekyll  https://eagle840.gitlab.io/jekyll/ 
 - My Resume is run the the Azure Devops pipellnes using npm resume-cli  http://nicholasbusby.vauxbuz.net/