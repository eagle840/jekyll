---
layout: page
title: Cheat Sheets
permalink: /cheatsheets/
---

The IR4 Engineers Cheat sheets

## Top cheat sheets
- [Bash vs CMD vs SP](https://eagle840.gitlab.io/jekyll/cli/)
- [ARM templating](https://eagle840.gitlab.io/jekyll/ARM/)
- [Docker](https://eagle840.gitlab.io/jekyll/docker/)
- [LLMs](https://eagle840.gitlab.io/jekyll/LLM/)


## Coding
- sheets 
  - link to sheets
  - link to sheets
- links
  - link 
  - link

### Python
- try <https://www.pythoncheatsheet.org/cheatsheet/basics>

### JavaSE
- sheets
  - link to sheets
- links
  - [jshell](https://docs.oracle.com/en/java/javase/19/jshell/introduction-jshell.html)
  - [monitoring-optimization-tools](https://seagence.com/blog/16-java-performance-monitoring-optimization-tools/)

### JavaEE
- sheets 
  - link to sheets
  - link to sheets
- links
  - [maven starter for EE](http://www.mastertheboss.com/java-ee/jakarta-ee/a-maven-starter-for-jakarta-ee-projects/)
  - [Master the Boss](http://www.mastertheboss.com/)

### useful websites

- websites
  - [Azure for Python Developers](https://learn.microsoft.com/en-us/azure/developer/python/)
  - [Azure various languages](https://learn.microsoft.com/en-us/azure/developer/)

- tools
  - cyber chef    https://gchq.github.io/CyberChef/
  

## Devops

### SRE

- sheets 
  - link to sheets
  - link to sheets
- links
  - [beeswithmachineguns](https://github.com/newsapps/beeswithmachineguns)
  - [10-cloud-tagging-best-practices](https://www.vitado.com/10-cloud-tagging-best-practices/)
  - [tagging](https://divvycloud.com/tagging-strategies-importance-and-utility-of-tagging-cloud-resources/)
  - [grafana university](https://university.grafana.com/learn/signin)
  - [opentelemtry](https://opentelemetry.io/registry/)

### Terraform

- sheets
- links
  - checkov [page](https://www.checkov.io/1.Welcome/What%20is%20Checkov.html)

### containization
- sheets 
  - link to sheets -docker, swarm
  - link to sheets -k8s
- links
  - [minikube](https://minikube.sigs.k8s.io/docs/start/)
  - [k3s storage](https://docs.k3s.io/storage)
  - [minikube vs kind vs k3](https://shipit.dev/posts/minikube-vs-kind-vs-k3s.html)
  - pick a k8s cluster [pdf](https://www.cncf.io/wp-content/uploads/2020/08/CNCF-Webinar-Navigating-the-Sea-of-Local-Clusters-.pdf)[youtube](https://www.youtube.com/watch?v=q6kyHDleioA)
  - [visualizer for Swarm](https://hub.docker.com/r/dockersamples/visualizer/#!)
  - [play-with-docker - 32Gb, 8core!](https://labs.play-with-docker.com/)
  - [play-with-k8s](https://labs.play-with-k8s.com/)
  - covert docker-compose to k8s service [kompose](https://kompose.io/getting-started/#minishift-and-kompose)
  - [k8s ingress](https://kind.sigs.k8s.io/docs/user/ingress/)
  - [docker security](https://cheatsheetseries.owasp.org/cheatsheets/Docker_Security_Cheat_Sheet.html)

### online tools
- [grok debugger](https://grokdebugger.com/)
- [gitignore generator](https://www.toptal.com/developers/gitignore)


## Data Engineering

### ElasticSearch

- training
  - [es beginners crash course V2  - JS ](https://dev.to/lisahjung/beginners-guide-to-building-a-full-stack-app-nodejs-react-with-elasticsearch-5347)
  - [main docs page: ](https://www.elastic.co/guide/en/elasticsearch/reference/8.5/elasticsearch-intro.html)
  - [community  ](https://cheatsheetseries.owasp.org/cheatsheets/Docker_Security_Cheat_Sheet.html)
  - [lab excerises  ](https://georgebridgeman.com/exercises/)
  - [Elasticsearch: The Definitive Guide](https://www.elastic.co/guide/en/elasticsearch/guide/current/index.html)
  - [reddit page](https://www.reddit.com/r/elasticsearch/)
  - [stackoverflow page](https://stackoverflow.com/questions/tagged/elasticsearch)
  - [https://discuss.elastic.co/](https://discuss.elastic.co/)
  - [subscription/license](https://www.elastic.co/subscriptions)
  - [painless ES language](https://www.elastic.co/guide/en/elasticsearch/reference/master/modules-scripting-painless.html)

### ML

- sheets 
  - link to sheets
  - link to sheets
  - my cheat sheet <https://eagle840.gitlab.io/jekyll/LLM/>
- links
  - [statquest](https://www.youtube.com/@statquest)
  - [Weights & Biases](https://wandb.ai/site) - need an account
  - [Hugging Face](https://huggingface.co/)
  - [Gradio](https://www.gradio.app/)
  - [Streamlit](https://streamlit.io/)
  - [deeplearning short courses](https://www.deeplearning.ai/short-courses/) or <https://learn.deeplearning.ai/>
  - [Google Genertive AI Developer](https://developers.generativeai.google/)


### Blogs & Useful websites
- Blogs
  - https://www.startdataengineering.com/

- Useful 
  - [dbt fundamentals](https://courses.getdbt.com/courses/fundamentals)

## Finance

- sheets 
  - link to sheets
  - link to sheets
- links
  - [dataroma](https://www.dataroma.com/m/allact.php?typ=a)
  - https://www.investopedia.com/terms/f/form-13f.asp 
  - https://whalewisdom.com/
  - https://simplywall.st/discover/community-ideas/293368/top-warren-buffett-stock-buys-q3?via=newmoney

## cyber
- sheets 
  - link to sheets
  - link to sheets
- links
  - [email](https://mxtoolbox.com/)
  - [search OPSWAT](https://metadefender.opswat.com/)
  - [virus total](https://www.virustotal.com/gui/home/upload)

## blank template
- sheets 
  - link to sheets
  - link to sheets
- links
  - link 
  - link
  


## Other CI/CD  
 - My Blog is run that the Gitlabs pipelines using Jekyll  https://eagle840.gitlab.io/jekyll/ 
 - My Resume is run the the Azure Devops pipellnes using npm resume-cli  http://nicholasbusby.vauxbuz.net/
